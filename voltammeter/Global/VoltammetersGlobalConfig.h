//
// Created by Xinyi Zhang on 2018/5/18.
// Copyright (c) 2018 汪宇豪. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 * 如果要调用Config，import该头文件，extern VoltammetersGlobalConfig *appConfig，全局config在main.m中创建
 */

#define BLUTOOTH_CONECTION 1
#define WIFI_CONECTION 2
#define DEFAULT_LOCAL_CONFIG_NAME @"latestConfig.js"

@interface VoltammetersGlobalConfig : NSObject

@property (strong, nonatomic) NSString *deviceName,*wifiID,*bluetoothID;
@property (assign, nonatomic) int connectionType,plugType;
@property (assign, nonatomic) double PTTransformationRatio,CTTransformationRatio,safeThreshold;

//获取配置jSON并返回对应的NS字典
-(NSDictionary *)getHistoryConfigFromFile:(NSString *)fileName;

//将参数设置，成功返回true
-(bool)setConfigWithNSDictionary:(NSDictionary *)jSONDiction;

//无文件初始化
-(void)initWithoutFile:(VoltammetersGlobalConfig *)newConfig;

-(bool)isPTTransformationRatioLegal:(NSString *) PT;

-(bool)isCTTransformationRatio:(NSString *) CT;

-(bool)isSafeThresholdLegal:(NSString *) safeThreshold;
//将参数生成NSDate格式的jSON数据
-(NSData *)createjSONData;
//保存当前配置于本地文件中(jSON格式）
-(bool)saveCurrentConfigToLocal:(NSData *)jSONCofig;


@end
