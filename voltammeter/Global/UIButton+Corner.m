//
//  UIButton+Corner.m
//  voltammeter
//
//  Created by 汪宇豪 on 2018/5/22.
//  Copyright © 2018 汪宇豪. All rights reserved.
//

#import "UIButton+Corner.h"

@implementation UIButton (Corner)
- (void)setCorner:(CGFloat)radius color:(UIColor *)color width:(CGFloat)width
{
    self.layer.cornerRadius = radius;
    self.layer.masksToBounds = YES;
    [self.layer setBorderColor:color.CGColor];
    [self.layer setBorderWidth:width];
}
@end
