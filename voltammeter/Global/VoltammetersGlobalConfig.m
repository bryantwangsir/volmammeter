//
// Created by Xinyi Zhang on 2018/5/18.
// Copyright (c) 2018 汪宇豪. All rights reserved.
//

#import "VoltammetersGlobalConfig.h"


@interface VoltammetersGlobalConfig ()
- (bool)isPTTransformationRatioLegal:(NSString *)PTTransformationRatio;
@end

@implementation VoltammetersGlobalConfig
//获取当前时间 yyyy-MM-dd HH:mm
-(NSString *)getCurrentDate
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSTimeZone *localTimeZone=[NSTimeZone systemTimeZone];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //获取当前的时间
    NSDate *currentDate=[NSDate new];
    currentDate=[currentDate dateByAddingTimeInterval:[localTimeZone secondsFromGMTForDate:currentDate]];
    NSString *currentDateStr = [NSString stringWithFormat:@"%@",currentDate];
    return currentDateStr;
}

-(NSDictionary *)getHistoryConfigFromFile:(NSString *)fileName
{
    //获取Document路径
    NSArray *docPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true);
    NSString *documentPath = [docPath objectAtIndex:0];
    NSString *configFilePath = [[documentPath stringByAppendingString:@"/"] stringByAppendingString:fileName];
    NSLog(@"%@",configFilePath);
    NSData *configData = [[NSData alloc] initWithContentsOfFile:configFilePath];
    if(configData)
    {
        //将对应jSON转换为NS字典
        NSDictionary *jSONConfig = [NSJSONSerialization JSONObjectWithData:configData options:nil error:nil];
        return jSONConfig;
    } else
    {
        return nil;
    }

}

-(bool)setConfigWithNSDictionary:(NSDictionary *)jSONDiction {
    if (jSONDiction == nil)
    {
        return false;
    } else{
        double CTValue=[[jSONDiction objectForKey:@"CT"] doubleValue],PTValue=[[jSONDiction objectForKey:@"PT"] doubleValue];
        double safeThresholdValue=[[jSONDiction objectForKey:@"safeThreshold"] doubleValue];
        int plugTypeValue = [[jSONDiction objectForKey:@"plugType"] intValue];
        self.deviceName=[jSONDiction objectForKey:@"device"];
        self.CTTransformationRatio=CTValue;
        self.PTTransformationRatio=PTValue;
        self.safeThreshold=safeThresholdValue;
        self.plugType=plugTypeValue;
        return true;
    }
}
-(void)initWithoutFile:(VoltammetersGlobalConfig *)newConfig {
    newConfig.deviceName=@"Voltammeter";
    newConfig.connectionType=WIFI_CONECTION;
    newConfig.PTTransformationRatio = 0.0;
    newConfig.CTTransformationRatio = 0.0;
    newConfig.safeThreshold= 0.0;
    newConfig.plugType = 0;
    newConfig.bluetoothID = @"无";
    newConfig.wifiID = @"无";
}

-(VoltammetersGlobalConfig *)init {
    if (self = [super init])
    {
        if ([self setConfigWithNSDictionary:[self getHistoryConfigFromFile:DEFAULT_LOCAL_CONFIG_NAME]] == false)
        {
            [self initWithoutFile:self];
            [self saveCurrentConfigToLocal:[self createjSONData]] ;
        }

    }
    return self;

}

-(bool)isCTTransformationRatio:(NSString *)CT {
    if ([CT doubleValue]==0)
    {
        return false;
    } else
    {
        return true;
    }
}

-(bool)isPTTransformationRatioLegal:(NSString *)PT {
    if ([PT doubleValue]==0)
    {
        return false;
    } else
    {
        return true;
    }
}

-(bool)isSafeThresholdLegal:(NSString *)safeThreshold {
    if ([safeThreshold doubleValue]==0)
    {
        return false;
    } else {
        return true;
    }
}

-(NSData *)createjSONData {
    //生成一个词典
    NSDictionary *configDic;
    configDic = [NSDictionary dictionaryWithObjectsAndKeys:self.deviceName, @"device",
            [NSString stringWithFormat:@"%lf", self.PTTransformationRatio], @"PT",
                    [NSString stringWithFormat:@"%lf", self.CTTransformationRatio], @"CT",
                            [NSString stringWithFormat:@"%lf", self.safeThreshold], @"safeThreshold",
                                    [NSString stringWithFormat:@"%d", self.plugType], @"plugType",
                                            [self getCurrentDate],@"time",
                                            nil];

    // isValidJSONObject判断对象是否可以构建成json对象
    if ([NSJSONSerialization isValidJSONObject:configDic]) {
        NSError *error;
        // 创造一个json从Data, NSJSONWritingPrettyPrinted指定的JSON数据产的空白，使输出更具可读性。
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:configDic options:NSJSONWritingPrettyPrinted error:&error];
        //NSString *json = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        //NSLog(@"json data:%@", json);
        return jsonData;
    } else
    {
        return nil;
    }
}

-(bool)saveCurrentConfigToLocal:(NSData *)jSONCofig {
        //获取Document文件夹地址
        NSArray *documentsPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docPath = [documentsPaths objectAtIndex:0];
        NSLog(@"path:%@", docPath);
        //创建文件
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *newFilePath = [[docPath stringByAppendingString:@"/"] stringByAppendingString:DEFAULT_LOCAL_CONFIG_NAME];
        if([fileManager createFileAtPath:newFilePath contents:jSONCofig attributes:nil])
        {
            return true;
        } else{
            return false;
        }

}

@end
