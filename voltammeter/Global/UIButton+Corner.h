//
//  UIButton+Corner.h
//  voltammeter
//
//  Created by 汪宇豪 on 2018/5/22.
//  Copyright © 2018 汪宇豪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Corner)
- (void) setCorner:(CGFloat )radius color:(UIColor*)color width:(CGFloat)width;
@end
