//
//  VoltammeterMeTableViewCell.m
//  voltammeter
//
//  Created by Xinyi Zhang on 2018/5/10.
//  Copyright © 2018年 汪宇豪. All rights reserved.
//

#import "VoltammeterMeTableViewCell.h"
#import "UIView+YYAdd.h"

@implementation VoltammeterMeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setTableViewWithIndexPath:(NSIndexPath *)indexpath {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.cellLable.font = [UIFont boldSystemFontOfSize:16.0];
    if (indexpath.section == 0 && indexpath.row == 0) {
        self.cellIcon.image = [UIImage imageNamed:@"voltammeter_mine_Record"];
        self.cellLable.text = @"测量记录";
    }else if (indexpath.section == 1 && indexpath.row == 0){
        self.cellIcon.image = [UIImage imageNamed:@"voltammeter_mine_connect"];
        self.cellLable.text = @"连接方式";
    }else if (indexpath.section == 1 && indexpath.row == 1){
        self.cellIcon.image = [UIImage imageNamed:@"voltammeter_mine_update"];
        self.cellLable.text = @"信息同步";
    }
}


@end
