//
//  TestHistoryTableViewCell.m
//  voltammeter
//
//  Created by Xinyi Zhang on 2018/5/22.
//  Copyright © 2018年 汪宇豪. All rights reserved.
//

#import "TestHistoryTableViewCell.h"

@implementation TestHistoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/*
-(void)setModel:(WiFiModel *)model{
    _model = model;
    NSString *testTime=[[ArresterGlobalManager sharedArresterGlobalManager]tranformLocalStringToDate:model.testTime];
    NSMutableAttributedString *tempStr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@\n%@",model.arrester,testTime]];
    [tempStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#67a0c6"] range:NSMakeRange(tempStr.length-20,20)];
    self.nameLabel.attributedText = tempStr;
    self.nameLabel.lineBreakMode = UILineBreakModeWordWrap;
    self.nameLabel.numberOfLines = 0 ;
    if (self.currentFileType == kLocalFileType) {
        if (model.isUpLoad == 0) {
            self.upLoadImageV.hidden = YES;
        }else{
            self.upLoadImageV.hidden = NO;
        }
    }
    else
    {
        self.upLoadImageV.hidden = YES;
    }
    
}
 */
-(void)setIsSelect:(BOOL)isSelect{
    _isSelect = isSelect;
    if (_isSelect) {
        self.voltammeterImageV.image = [UIImage imageNamed:@"voltammeter_selectHigh"];
    }else{
        self.voltammeterImageV.image = [UIImage imageNamed:@"voltammeter_selectNormal"];
    }
}




- (IBAction)selectBtnClick:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(TestHistoryTableViewCellSelectBtnClick:)]) {
        [self.delegate TestHistoryTableViewCellSelectBtnClick:self];
    }
}
@end
