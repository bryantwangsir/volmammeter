//
//  VoltammeterMeTableViewCell.h
//  voltammeter
//
//  Created by Xinyi Zhang on 2018/5/10.
//  Copyright © 2018年 汪宇豪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VoltammeterMeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *cellIcon;
@property (weak, nonatomic) IBOutlet UILabel *cellLable;

-(void)setTableViewWithIndexPath:(NSIndexPath *)indexpath;

@end
