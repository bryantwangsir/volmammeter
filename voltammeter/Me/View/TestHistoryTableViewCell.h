//
//  TestHistoryTableViewCell.h
//  voltammeter
//
//  Created by Xinyi Zhang on 2018/5/22.
//  Copyright © 2018年 汪宇豪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"

@class TestHistoryTableViewCell;

@protocol TestHistoryTableViewCellDelegate <NSObject>

-(void)TestHistoryTableViewCellSelectBtnClick:(TestHistoryTableViewCell *)testCell;

@end

@interface TestHistoryTableViewCell : UITableViewCell

@property(nonatomic,weak) id<TestHistoryTableViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *voltammeterImageV;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UIImageView *arrowImageV;


//@property(nonatomic,strong) WiFiModel *model;

//@property(nonatomic,strong) WiFiSettingModel *setModel;
//是否为选中
@property(nonatomic,assign) BOOL isSelect;

@property (weak, nonatomic) IBOutlet UIButton *selectBtn;

@property (weak, nonatomic) IBOutlet UIImageView *upLoadImageV;

@property(nonatomic,assign) FileTypeStatus currentFileType;

- (IBAction)selectBtnClick:(id)sender;


@end
