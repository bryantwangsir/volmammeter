//
//  VoltammeterMeTableViewController.h
//  voltammeter
//
//  Created by Xinyi Zhang on 2018/5/10.
//  Copyright © 2018年 汪宇豪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VoltammeterMeTableViewController : UITableViewController
-(void) setUpStartView;
@end
