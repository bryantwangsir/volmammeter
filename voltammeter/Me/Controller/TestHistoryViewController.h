//
//  TestHistoryViewController.h
//  voltammeter
//
//  Created by Xinyi Zhang on 2018/5/16.
//  Copyright © 2018年 汪宇豪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TestHistoryTableViewCell.h"
#import "Header.h"

@interface TestHistoryViewController : UIViewController<TestHistoryTableViewCellDelegate,UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *localBtn;
@property (weak, nonatomic) IBOutlet UIButton *webBtn;

@property (weak, nonatomic) IBOutlet UIView *dateContainView;

@property (weak, nonatomic) IBOutlet UITableView *TestHistoryTableView;

//@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *datePickerContainConstantBottom;

@property (weak, nonatomic) IBOutlet UIButton *choseAllBtn;
@property (weak, nonatomic) IBOutlet UIButton *upLoadBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIButton *downloadBtn;
@property (weak, nonatomic) IBOutlet UIButton *webChoseAll;
@property (weak, nonatomic) IBOutlet UITextField *sercahtextField;

@property (weak, nonatomic) IBOutlet UILabel *dataLabel;

@property(nonatomic,assign) FileTypeStatus currentFileType;
@property (weak, nonatomic) IBOutlet UIImageView *webIcon;
@property (weak, nonatomic) IBOutlet UIImageView *localIcon;


@property (nonatomic,strong) NSMutableArray *LocalAllArray;

@property (nonatomic,strong) NSMutableArray *LocalTestArray;

@property(nonatomic,strong) NSMutableArray *webTestArray;

@property(nonatomic,strong) NSMutableArray *webConfigArray;

@property(nonatomic,strong) NSMutableArray *LocalSelctArr;

@property(nonatomic,strong) NSMutableArray *webSelectTestArray;

@property(nonatomic,strong) NSMutableArray *webSelectConfigArray;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UIButton *startDateBtn;
@property (weak, nonatomic) IBOutlet UIButton *endDateBtn;
@property (weak, nonatomic) IBOutlet UIButton *dateSearchBtn;

@property(nonatomic,assign) BOOL isShowDatePicker;
@property(nonatomic,assign) BOOL isShowLastDate;//开始日期
@property(nonatomic,assign) BOOL isShowCurrentDate;//结束日期
@property(nonatomic,strong) NSDate *lastDate;//开始日期
@property(nonatomic,strong) NSDate *endDate;//结束日期

@property(nonatomic,assign) NSInteger pageIndex;//显示当前页数

@property(nonatomic,strong) NSDictionary *metaTemp;//暂存meta的字典

@property (nonatomic,strong) NSMutableArray *LocalConfigArray;

@property (nonatomic,assign) BOOL isInner;

@end
