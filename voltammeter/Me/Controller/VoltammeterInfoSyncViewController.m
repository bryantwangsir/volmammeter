//
//  VoltammeterInfoSyncViewController.m
//  voltammeter
//
//  Created by Xinyi Zhang on 2018/5/12.
//  Copyright © 2018年 汪宇豪. All rights reserved.
//

#import "VoltammeterInfoSyncViewController.h"

extern VoltammetersGlobalConfig * appConfig;

@interface VoltammeterInfoSyncViewController ()
{
    //NSString *currentWifiStr;
    NSString *wifiStr;
    NSString *idStr;
    NSString *bleStr;
}

@end

@implementation VoltammeterInfoSyncViewController

- (void)viewDidLoad {
    [self setupStartView];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupStartView
{
    UIImage *modifyBtnIcon;
    if(appConfig.wifiID)
    {
        wifiStr=appConfig.wifiID;
    } else{
        wifiStr=@"无WiFi连接";
    }
    if(appConfig.bluetoothID)
    {
        bleStr=appConfig.bluetoothID;
    } else{
        bleStr=@"无蓝牙连接";
    }
    idStr=appConfig.deviceName;
    NSString  *wifiStr1 = @"当前设备wifi:";
    NSString *bleStr1 = @"当前设备BLE id:";
    NSString *idStr1 = @"当前设备ID:";
    modifyBtnIcon=[UIImage imageNamed:@"voltammeter_modify_info"];
    [_modifyBtn setImage:modifyBtnIcon forState:UIControlStateNormal];
    _deviceId.text=[idStr1 stringByAppendingString:idStr];
    _wifiID.text=[wifiStr1 stringByAppendingString:wifiStr];
    _bluetoothID.text= [bleStr1 stringByAppendingString:bleStr];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)modifyBtn_Action:(id)sender {
}

- (IBAction)syncBtn_Action:(id)sender {
    NSLog(@"%@",appConfig.wifiID);
    [self setupStartView];
}
@end
