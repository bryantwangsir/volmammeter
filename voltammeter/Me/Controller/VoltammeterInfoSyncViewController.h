//
//  VoltammeterInfoSyncViewController.h
//  voltammeter
//
//  Created by Xinyi Zhang on 2018/5/12.
//  Copyright © 2018年 汪宇豪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VoltammetersGlobalConfig.h"

@interface VoltammeterInfoSyncViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *modifyBtn;
@property (weak, nonatomic) IBOutlet UILabel *deviceId;
@property (weak, nonatomic) IBOutlet UILabel *wifiID;
@property (weak, nonatomic) IBOutlet UILabel *bluetoothID;
- (IBAction)modifyBtn_Action:(id)sender;
- (IBAction)syncBtn_Action:(id)sender;

@end
