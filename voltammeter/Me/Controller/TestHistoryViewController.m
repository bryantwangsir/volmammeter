//
//  TestHistoryViewController.m
//  voltammeter
//
//  Created by Xinyi Zhang on 2018/5/16.
//  Copyright © 2018年 汪宇豪. All rights reserved.
//

#import "TestHistoryViewController.h"


BOOL isLocalHistory = true;

@interface TestHistoryViewController ()

@end

@implementation TestHistoryViewController
-(void)setupStartView
{
    [self.view bringSubviewToFront:_dateContainView];
    if (isLocalHistory) {
        _localIcon.image = [UIImage imageNamed:@"voltammeter_local_high"]; 
        _webIcon.image = [UIImage imageNamed:@"voltammeter_network_normal"];
    }
    else
    {
        _localIcon.image = [UIImage imageNamed:@"voltammeter_local_normal"];
        _webIcon.image = [UIImage imageNamed:@"voltammeter_network_high"];
    }
    self.navigationController.navigationBar.translucent = NO;
    self.tabBarController.tabBar.translucent = NO;
    UIView *footerView = [[UIView alloc] init];
    UIView *view = [[UIView alloc] init];
    self.endDate = [NSDate date];
    self.TestHistoryTableView.tableFooterView = footerView;
    self.TestHistoryTableView.backgroundView = view;
    self.datePicker.datePickerMode =  UIDatePickerModeDate;
    [self.TestHistoryTableView registerNib:[UINib nibWithNibName:@"TestHistoryTableViewCell" bundle:nil] forCellReuseIdentifier:@"kTestHistoryIdentifier"];
    self.currentFileType = kLocalFileType;
    [self setSelectBntWithFileType:self.currentFileType];
    self.localBtn.selected = YES;
    self.webBtn.selected=NO;
    
   // self.LocalTestArray = [NSMutableArray arrayWithArray:[[ArresterDataBase shareDataBase]getAllTestHistory]];
    __weak typeof(self) weakSelf = self;
   // [ArresterCheckDownModel checkUpLoadStatusWithFromInfoArr:self.LocalTestArray.copy macStr:[SystemWifiModel getCurrentSystemWifi].mac blue:nil successBlock:^(NSArray *sucArr) {
    /*
    //重新获取本地文件是否为已经上传过的
        weakSelf.LocalTestArray = [NSMutableArray arrayWithArray:[[ArresterDataBase shareDataBase]getAllTestHistory]];
        weakSelf.LocalAllArray = [weakSelf.LocalTestArray mutableCopy];
        [weakSelf.TestHistoryTableView reloadData];
} errorBlock;^(BaseModel *er) {}];
    */
    
    NSString *currentDateString = @"2016-01-01 ";
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:@"yyyy-MM-DD "];
    [dateFormater setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    self.lastDate = [dateFormater dateFromString:currentDateString];
    NSString *startBtn = [NSString stringWithFormat:@"%@",self.lastDate];
    NSRange rg1 = [startBtn rangeOfString:@" "];
    startBtn = [startBtn substringToIndex:rg1.location];
    [self.startDateBtn setTitle:startBtn forState:UIControlStateNormal];
    
    
    
    //获取当前的时间
    NSString *currentDateStr = [NSString stringWithFormat:@"%@",self.endDate];
    NSRange rg = [currentDateStr rangeOfString:@" " options:NSCaseInsensitiveSearch];
    currentDateStr = [currentDateStr substringToIndex:rg.location];
    [self.endDateBtn setTitle:currentDateStr forState:UIControlStateNormal];
    
    //和下拉刷新相关部分
    self.pageIndex=1;
    self.metaTemp=[NSDictionary dictionaryWithObjectsAndKeys:@"1",@"currentPage",@"1",@"pageCount",@"20",@"perPage",@"0",@"totalCount", nil];
    
}

- (void)viewDidLoad {
    [self setupStartView];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)localBtnClick:(id)sender {
    self.currentFileType = kLocalFileType;
    [self setSelectBntWithFileType:self.currentFileType];
    [self.localIcon setImage:[UIImage imageNamed:@"voltammeter_local_high"]];
    [self.webIcon setImage:[UIImage imageNamed:@"voltammeter_network_normal"]];
    if (self.LocalSelctArr.count != 0 && self.LocalSelctArr.count == self.LocalTestArray.count) {
        [self.choseAllBtn setTitle:@"取消全选" forState:UIControlStateNormal];
    }
    else
    {
        [self.choseAllBtn setTitle:@"全选" forState:UIControlStateNormal];
        
    }
    [self.TestHistoryTableView reloadData];
}
- (IBAction)webBtnClick:(id)sender {
    self.currentFileType = kWebFileType;
    [self setSelectBntWithFileType:self.currentFileType];
    [self.localIcon setImage:[UIImage imageNamed:@"voltammeter_local_normal"]];
    [self.webIcon setImage:[UIImage imageNamed:@"voltammeter_network_high"]];
    if (self.webSelectConfigArray.count != 0 && self.webSelectConfigArray.count == self.webConfigArray.count) {
        [self.webChoseAll setTitle:@"取消全选" forState:UIControlStateNormal];
    }
    else
    {
        [self.webChoseAll setTitle:@"全选" forState:UIControlStateNormal];
        
    }
}
- (IBAction)webChodeAllBtnClick:(id)sender {
    if (self.webSelectTestArray.count == self.webTestArray.count) {
        [self.webChoseAll setTitle:@"全选" forState:UIControlStateNormal];
        [self.webSelectTestArray removeAllObjects];
        [self.webSelectConfigArray removeAllObjects];
    }else{
        if (self.webSelectTestArray.count != 0) {
            [self.webSelectTestArray removeAllObjects];
        }
        [self.webSelectTestArray addObjectsFromArray:self.webTestArray.copy];
        [self.webSelectConfigArray addObjectsFromArray:self.webConfigArray.copy];
        [self.webChoseAll setTitle:@"取消全选" forState:UIControlStateNormal];
    }
    [self.TestHistoryTableView reloadData];
}
- (IBAction)choseAllBtnClick:(id)sender {
    if (self.LocalSelctArr.count == self.LocalTestArray.count) {
        [self.choseAllBtn setTitle:@"全选" forState:UIControlStateNormal];
        [self.LocalSelctArr removeAllObjects];
    }else{
        if (self.LocalSelctArr.count != 0) {
            [self.LocalSelctArr removeAllObjects];
        }
        [self.LocalSelctArr addObjectsFromArray:self.LocalTestArray.copy];
        [self.choseAllBtn setTitle:@"取消全选" forState:UIControlStateNormal];
        
    }
    [self.TestHistoryTableView reloadData];
}
- (IBAction)downloadBtnClick:(id)sender {
   /* if (self.webSelectTestArray.count == 0) {
        [SVProgressHUD showInfoWithStatus:@"请选择要下载的内容"];
    }else{
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            //筛选已经下载过的数据
            [SVProgressHUD showWithStatus:@"正在下载"];
            NSInteger isDownload = FALSE;
            for (int i = 0; i < self.webSelectTestArray.count; i++) {
                WiFiModel *model = self.webSelectTestArray[i];
                WiFiModel *searchModel = [[ArresterDataBase shareDataBase] getLocalTestWithTestUid:model.testUUID];
                
                //如果本地不存在这条记录
                if (!searchModel) {
                    [[ArresterDataBase shareDataBase] saveDownloadTestWithWiFiModel:model WiFiSettingModel:self.webSelectConfigArray[i]];
                    [self.LocalTestArray addObject:model];
                    [self.LocalAllArray addObject:model];
                }
                else
                {
                    isDownload = YES;
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (isDownload) {
                    [SVProgressHUD showSuccessWithStatus:@"某些记录已下载，将自动跳过!"];
                }
                else
                {
                    [SVProgressHUD showSuccessWithStatus:@"保存成功!"];
                }
                [self.webSelectTestArray removeAllObjects];
                [self.webSelectConfigArray removeAllObjects];
                [self.webChoseAllBtn setTitle:@"全选" forState:UIControlStateNormal];
                [self setControllerTitle];
                [self.TestHistoryTableView reloadData];
            });
        });
    }*/
}

- (IBAction)uploadBtnClick:(id)sender {
}

- (IBAction)deletBtnClick:(id)sender {
    if (self.LocalSelctArr.count == 0) {
       // [SVProgressHUD showInfoWithStatus:@"请选择要删除的内容"];
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"确定删除选中的记录吗？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alertView show];
    }
}

- (IBAction)startDateBtnClick:(id)sender {
    [self.view endEditing:YES];
    self.isShowDatePicker = !self.isShowDatePicker;
    if (self.isShowDatePicker) {
        self.isShowLastDate = true;
        [UIView animateWithDuration:0.5 animations:^{
            self.datePickerContainConstantBottom.constant = 0;
            //[self.datePicker setDate:self.lastDate];
            self.dataLabel.text=@"开始日期";
        }];
    }else{
        self.isShowLastDate = NO;
        [UIView animateWithDuration:0.5 animations:^{
            self.datePickerContainConstantBottom.constant = 300;
        }];
    }
}

//结束日期
- (IBAction)endDateBtnClick:(id)sender {
    self.isShowCurrentDate = !self.isShowCurrentDate;
    [self.view endEditing:YES];
    if (self.isShowCurrentDate) {
        self.isShowCurrentDate= YES;
        [UIView animateWithDuration:0.5 animations:^{
            self.datePickerContainConstantBottom.constant = 0;
            //[self.datePicker setDate:self.endDate];
            self.dataLabel.text=@"结束日期";
        }];
    }else{
        self.isShowCurrentDate= NO;
        [UIView animateWithDuration:0.5 animations:^{
            self.datePickerContainConstantBottom.constant = 300;
        }];
    }
}

//包括日期搜索和事件搜索
- (IBAction)dateSearchBtnClick:(id)sender {
   /* //本地日期搜索和线上日期搜索
    [self.searchtextField resignFirstResponder];
    if (self.currentFileType == kLocalFileType) {
        [self getAllSelectStartDate:self.lastDate endDate:self.endDate searchText:self.searchtextField.text];
    }else{// 线上的日期搜索
        if([ArresterGlobalManager sharedArresterGlobalManager].isConnectNetwork == YES){
            if([[[ArresterGlobalManager sharedArresterGlobalManager]getIsSynchronized] isEqualToString: @"YES"])
            {
                __weak typeof(self) weakSelf = self;
                ;
                [SVProgressHUD showWithStatus:@"正在搜索"];
                [TTWebSearchModel getWebSearchHistoryWithRecordName:[[ArresterGlobalManager sharedArresterGlobalManager]getEquipmentMac] macSsid:[[ArresterGlobalManager sharedArresterGlobalManager]getEquipmentSSid] blueMac:[[ArresterGlobalManager sharedArresterGlobalManager]getBlueMac] blueSsid:[[ArresterGlobalManager sharedArresterGlobalManager]getBlueSSid] pageNeed:nil startTime:[NSString stringWithFormat:@"%@",self.lastDate] endTime:[NSString stringWithFormat:@"%@",self.endDate] arresterName:self.searchtextField.text successBlock:^(NSArray *historyTestArr,NSArray *configTestArr,NSDictionary *_meta) {
                    if (weakSelf.webTestArray.count != 0) {
                        [weakSelf.webTestArray removeAllObjects];
                    }
                    if (weakSelf.webConfigArray.count != 0) {
                        [weakSelf.webConfigArray removeAllObjects];
                    }
                    // [weakSelf.webSelectTestArray removeAllObjects];
                    [weakSelf.webTestArray addObjectsFromArray:historyTestArr];
                    [weakSelf.webConfigArray addObjectsFromArray:configTestArr];
                    weakSelf.metaTemp = _meta;
                    [SVProgressHUD dismiss];
                    [self setControllerTitle];
                    [weakSelf.TestHistoryTableView reloadData];
                    self.TestHistoryTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
                } errorBlock:^(BaseModel *erModel) {}];
                ;
            }
            else{
                [SVProgressHUD showErrorWithStatus:@"当前没有连接过任何设备，无法搜索网络数据!"];
            }
        }
        else
        {
            [SVProgressHUD showErrorWithStatus:@"当前无可用网络!"];
        }
    }
    [self setControllerTitle];
    */
}
/*
//本地的历史时间记录搜索
-(void)getAllSelectStartDate:(NSDate *)startDate endDate:(NSDate *)endDate searchText:(NSString *)searchText{
    NSInteger startIndex = [startDate timeIntervalSince1970];
    NSInteger endIndex = [endDate timeIntervalSince1970];
    int daySeconds = 24 * 60 * 60;
    endIndex = endIndex / daySeconds;
    startIndex = startIndex / daySeconds;
    //日期搜索
    NSMutableArray *timeArrM = [NSMutableArray arrayWithCapacity:5];
    self.LocalTestArray = [NSMutableArray arrayWithArray:[[ArresterDataBase shareDataBase]getAllTestHistory]];
    if (startDate) {//时间存在
        for (int i = 0; i < self.LocalTestArray.count; i++) {
            WiFiModel *model = self.LocalTestArray[i];
            NSInteger recordTime;
            recordTime = model.testTime.intValue / daySeconds;
            if (recordTime >= startIndex && recordTime <= endIndex) {
                [timeArrM addObject:model];
            }
        }
    }
    //关键字搜索
    NSMutableArray *searTextArr = [NSMutableArray array];
    if (searchText.length != 0) {
        searTextArr = [NSMutableArray arrayWithArray:[[ArresterDataBase shareDataBase] getTestHistoryWithSearchNameString:searchText]];
    }
    if (startDate && searchText.length == 0) {//仅仅时间
        if (self.LocalTestArray.count != 0) {
            [self.LocalTestArray removeAllObjects];
        }
        [self.LocalTestArray addObjectsFromArray:timeArrM.copy];
        [self.TestHistoryTableView reloadData];
    }else if (searchText.length != 0 && startDate){
        NSMutableArray *totalArr = [NSMutableArray array];
        for (int i = 0; i < timeArrM.count; i++) {
            WiFiModel *model0 = timeArrM[i];
            for (int j = 0; j <searTextArr.count; j++) {
                WiFiModel *model1 = searTextArr[j];
                if ([model0.testUUID isEqualToString:model1.testUUID]) {
                    [totalArr addObject:model1];
                }
            }
        }
        if (self.LocalTestArray.count != 0) {
            [self.LocalTestArray removeAllObjects];
        }
        [self.LocalTestArray addObjectsFromArray:totalArr.copy];
        [self.TestHistoryTableView reloadData];
    }
    [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"搜索成功，共有%lu条数据", (unsigned long)self.LocalTestArray.count]];
}
*/

- (IBAction)datePickerCancel:(id)sender {
    [UIView animateWithDuration:0.5 animations:^{
        self.datePickerContainConstantBottom.constant = 300;
        self.isShowLastDate = NO;
        self.isShowCurrentDate = NO;
    }];
    
}

- (IBAction)datePickerSure:(id)sender {
    if (self.isShowLastDate) {//开始日期
        self.lastDate = self.datePicker.date;
        NSString *startBtn = [NSString stringWithFormat:@"%@",self.lastDate];
        NSRange rg = [startBtn rangeOfString:@" "];
        startBtn = [startBtn substringToIndex:rg.location];
        [self.startDateBtn setTitle:startBtn forState:UIControlStateNormal];
        [UIView animateWithDuration:0.5 animations:^{
            self.datePickerContainConstantBottom.constant = 300;
            self.isShowLastDate = NO;
        }];
    }else if (self.isShowCurrentDate){//结束日期
        self.endDate = self.datePicker.date;
        NSString *startBtn = [NSString stringWithFormat:@"%@",self.endDate];
        NSRange rg = [startBtn rangeOfString:@" "];
        startBtn = [startBtn substringToIndex:rg.location];
        [self.endDateBtn setTitle:startBtn forState:UIControlStateNormal];
        [UIView animateWithDuration:0.5 animations:^{
            self.datePickerContainConstantBottom.constant = 300;
            self.isShowCurrentDate = NO;
        }];
    }
}
/*
-(void)loadMore
{
    //if(self.metaTemp.page)
    NSString *currentPage=[self.metaTemp objectForKey:@"currentPage"];
    NSInteger currentPageNum = [currentPage intValue];
    NSString *totalPage=[self.metaTemp objectForKey:@"pageCount"];
    NSInteger totalPageNum = [totalPage intValue];
    NSString *totalCount=[self.metaTemp objectForKey:@"totalCount"];
    NSInteger totalCountNum = [totalCount intValue];
    
    NSLog(@"\n\n\ntotalpageNum : %ld  currentPageNum: %ld\n\n\n",totalPageNum,currentPageNum);
    if(totalPageNum>1&&currentPageNum<totalPageNum)
    {
        __weak typeof(self) weakSelf = self;
        self.pageIndex++;
        NSString *temps = [NSString stringWithFormat:@"%ld",self.pageIndex];
        [TTWebSearchModel getWebSearchHistoryWithRecordName:[[ArresterGlobalManager sharedArresterGlobalManager] getEquipmentMac] macSsid:[[ArresterGlobalManager sharedArresterGlobalManager] getEquipmentSSid] blueMac:[[ArresterGlobalManager sharedArresterGlobalManager]getBlueMac] blueSsid:[[ArresterGlobalManager sharedArresterGlobalManager]getBlueSSid] pageNeed:[NSString stringWithFormat:@"%@",temps] startTime:[NSString stringWithFormat:@"%@",self.lastDate] endTime:[NSString stringWithFormat:@"%@",self.endDate] arresterName:self.searchtextField.text successBlock:^(NSArray *historyTestArr,NSArray *configTestArr,NSDictionary *_meta) {
            if ([weakSelf.webChoseAllBtn.titleLabel.text isEqualToString:@"取消全选"]) {
                [self.webChoseAllBtn setTitle:@"全选" forState:UIControlStateNormal];
                [self.webSelectTestArray removeAllObjects];
                [self setControllerTitle];
            }
            [weakSelf.webTestArray addObjectsFromArray:historyTestArr];
            [weakSelf.webConfigArray addObjectsFromArray:configTestArr];
            weakSelf.metaTemp = _meta;
            //            NSLog(@"\n\n\n\n\n\n\n\nThis is dictionary meta!!!%@\n\n\n\n\n\n",_meta);
            [weakSelf.TestHistoryTableView reloadData];
            
        } errorBlock:^(BaseModel *erModel) {}];
        [self.TestHistoryTableView.mj_footer endRefreshing];
    }
    else
    {
        //        NSLog(@"\n\n\n\n\n\n\n已经load完了！！！！！\n\n\n\n\n\n\n\n");
        [self.TestHistoryTableView.mj_footer endRefreshingWithNoMoreData];
        [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"搜索成功，共有%lu条数据", (unsigned long)totalCountNum]];
        //        NSLog(@"\n\n\n\n\n\n\n %ld \n\n\n\n\n\n",(long)self.TestHistoryTableView.mj_footer.state);
    }
    
}*/

-(void)setSelectBntWithFileType:(FileTypeStatus)fileType{
    if (fileType == kLocalFileType) {
        self.localBtn.selected = YES;
        self.webBtn.selected = NO;
        self.deleteBtn.hidden = NO;
        self.choseAllBtn.hidden = NO;
        self.upLoadBtn.hidden = NO;
        self.webChoseAll.hidden = YES;
        self.downloadBtn.hidden = YES;
    }else{
        self.localBtn.selected = NO;
        self.webBtn.selected = YES;
        self.deleteBtn.hidden = YES;
        self.choseAllBtn.hidden = YES;
        self.upLoadBtn.hidden = YES;
        self.webChoseAll.hidden = NO;
        self.downloadBtn.hidden = NO;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.currentFileType == kLocalFileType) {
        return self.LocalTestArray.count;
    }else{
        return self.webTestArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TestHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"kTestHistoryIdentifier"];
    /*
    WiFiModel *model = [WiFiModel new];
    if (self.currentFileType == kLocalFileType) {
        model = self.LocalTestArray[indexPath.row];
        //        if ([self.LocalSelctArr containsObject:model]) {
        if([self isChosenModelInSelArr:_LocalSelctArr chosenModel:model]){
            cell.isSelect = YES;
        }else{
            cell.isSelect = NO;
        }
    }else{
        model =self.webTestArray[indexPath.row];
        WiFiSettingModel* setModel = self.webConfigArray[indexPath.row];
        cell.setModel = setModel;
        //        if ([self.webSelectTestArray containsObject:model]) {
        if([self isChosenModelInSelArr:_webSelectTestArray chosenModel:model]){
            cell.isSelect = YES;
        }else{
            cell.isSelect = NO;
        }
    }
     */
    cell.currentFileType = self.currentFileType;
    //cell.model = model;
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

/*
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    WiFiModel *model = [WiFiModel new];
    WiFiSettingModel *settingModel = [WiFiSettingModel new];
    if (self.currentFileType == kLocalFileType) {
        model = self.LocalTestArray[indexPath.row];
        settingModel=[[ArresterDataBase shareDataBase]getWifiSettingMoldeWithTestUUID:model.testUUID];
    }else{
        model = self.webTestArray[indexPath.row];
        settingModel = self.webConfigArray[indexPath.row];
    }
    ShowTestHistoryViewController *showHistoryVc = [[ShowTestHistoryViewController alloc] initWithNibName:@"ShowTestHistoryViewController" bundle:nil];
    showHistoryVc.wifiModel = model;
    showHistoryVc.wifiSettingModel = settingModel;
    [self.navigationController pushViewController:showHistoryVc animated:YES];
}
*/
/*
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    WiFiSettingModel *setModel = [WiFiSettingModel new];
    if (self.currentFileType == kLocalFileType) {
        setModel = self.LocalTestArray[indexPath.row];
    }else{
        setModel = self.webTestArray[indexPath.row];
    }
    NSString *tempStr = [NSString stringWithFormat:@"%@                %@",setModel.arrester,[[ArresterGlobalManager sharedArresterGlobalManager]tranformLocalStringToDate:setModel.testTime]];
    CGSize size = [tempStr sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0]}];
    int num = ceil(size.width / (screen_W - 100) + 0.5);
    if (num > 1) {
        return num * 31;
    }else{
        return kISiPad? 60 : 44;
    }
}


-(BOOL)isChosenModelInSelArr:(NSMutableArray *)selectArr chosenModel:(WiFiModel *)model{
    for(int i = 0; i < selectArr.count ; i++)
    {
        WiFiModel *model0 = selectArr[i];
        if([model0.testUUID isEqualToString:model.testUUID])
        {
            return YES;
        }
    }
    return NO;
}
*/
/*
-(void)TestHistoryTableViewCellSelectBtnClick:(TestHistoryTableViewCell *)testTableViewCell{
    WiFiModel *model = testTableViewCell.model;
    if (self.currentFileType == kLocalFileType) {//本地文件
        if(![self isChosenModelInSelArr:self.LocalSelctArr chosenModel:model]){
            [self.LocalSelctArr addObject:model];
        }else if([self isChosenModelInSelArr:self.LocalSelctArr chosenModel:model]){
            for(int i = 0; i < self.LocalSelctArr.count ; i++)
            {
                WiFiModel *model0 = self.LocalSelctArr[i];
                if([model.testUUID isEqualToString:model0.testUUID])
                {
                    [self.LocalSelctArr removeObject:model0];
                }
            }
        }
        if (self.LocalSelctArr.count == self.LocalTestArray.count) {
            [self.choseAllBtn setTitle:@"取消全选" forState:UIControlStateNormal];
        }else{
            [self.choseAllBtn setTitle:@"全选" forState:UIControlStateNormal];
        }
        [self setControllerTitle];
    }else{//网络文件
        if(![self isChosenModelInSelArr:self.webSelectTestArray chosenModel:model]){
            [self.webSelectTestArray addObject:model];
            [self.webSelectConfigArray addObject:testTableViewCell.setModel];
        }else if([self isChosenModelInSelArr:self.webSelectTestArray chosenModel:model]){
            for(int i = 0; i < self.webSelectTestArray.count ; i++)
            {
                WiFiModel *model0 = self.webSelectTestArray[i];
                if([model.testUUID isEqualToString:model0.testUUID])
                {
                    [self.webSelectTestArray removeObject:model0];
                    [self.webSelectConfigArray removeObject:testTableViewCell.setModel];
                }
            }
            //            [self.webSelectTestArray removeObject:model];
            //[self.webSelectConfigArray removeObject:testTableViewCell.setModel];
        }
        if (self.webSelectTestArray.count == self.webTestArray.count) {
            [self.webChoseAllBtn setTitle:@"取消全选" forState:UIControlStateNormal];
        }else{
            [self.webChoseAllBtn setTitle:@"全选" forState:UIControlStateNormal];
        }
        [self setControllerTitle];
    }
    [self.TestHistoryTableView reloadData];
    
}
*/
/*
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            [SVProgressHUD showWithStatus:@"正在删除"];
            [[ArresterDataBase shareDataBase]deleteTestHistoryInfoArr:self.LocalSelctArr];
            [self.LocalTestArray removeObjectsInArray:self.LocalSelctArr.copy];
            [self.LocalSelctArr removeAllObjects];
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showInfoWithStatus:@"删除成功"];
                [self setControllerTitle];
                [self.TestHistoryTableView reloadData];
                [self.choseAllBtn setTitle:@"全选" forState:UIControlStateNormal];
            });
        });
        
    }
}
*/
/*
//上传成功后，更新本地已经上传的数据显示
-(void)transformUpLoadStatesWithSucArr:(NSArray *)sucArr{
    for (int i = 0; i < sucArr.count; i++) {
        NSString *testUid = sucArr[i];
        [[ArresterDataBase shareDataBase] upDateDownLoadStatusWithMac:nil TestUUID:testUid];
        if (self.LocalTestArray.count != 0) {
            [self.LocalTestArray removeAllObjects];
        }
        self.LocalTestArray = [NSMutableArray arrayWithArray:[[ArresterDataBase shareDataBase]getAllTestHistory]];
        [self.TestHistoryTableView reloadData];
    }
}
 */
@end
