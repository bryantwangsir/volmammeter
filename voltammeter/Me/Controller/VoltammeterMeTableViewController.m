//
//  VoltammeterMeTableViewController.m
//  voltammeter
//
//  Created by Xinyi Zhang on 2018/5/10.
//  Copyright © 2018年 汪宇豪. All rights reserved.
//

#import "VoltammeterMeTableViewController.h"
#import "VoltammeterMeTableViewCell.h"
#import "TestHistoryViewController.h"
#import "VoltammterConnectionViewController.h"
#import "VoltammeterInfoSyncViewController.h"

#define TABLE_VIEW_SECTION_NUM 2
#define TABLE_VIEW_ROW_IN_SECTION_0 1
#define TABLE_VIEW_ROW_IN_SECTION_1 2
/*
 #define TABLE_VIEW_ROW_IN_SECTION_3 2
 #define TABLE_VIEW_ROW_IN_SECTION_4 1
 */

@interface VoltammeterMeTableViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *mineTableView;

@end

@implementation VoltammeterMeTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpStartView];
    
    // Do any additional setup after loading the view from its nib.
}

//获取cell的信息
-(void) setUpStartView
{
    self.title = @"我的";
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    UIView *footerView =[[UIView alloc] init];
    self.mineTableView.tableFooterView = footerView;
    [self.mineTableView registerNib:[UINib nibWithNibName:@"VoltammeterMeTableViewCell" bundle:nil] forCellReuseIdentifier:@"kMineCellIndentifier"];
    self.navigationController.navigationBar.translucent = NO;
    self.tabBarController.tabBar.translucent = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//tableView中的section
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return TABLE_VIEW_SECTION_NUM;
}

//返回每个section的行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return TABLE_VIEW_ROW_IN_SECTION_0;
    }else if (section == 1){
        return TABLE_VIEW_ROW_IN_SECTION_1;
    }
    else
    {
        return 1;
    }
}

//获得对应cell
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identidier = @"kMineCellIndentifier";
    VoltammeterMeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identidier];
    if(!cell)
    {
        cell = [[VoltammeterMeTableViewCell alloc] init];
    }
    [cell setTableViewWithIndexPath:indexPath];
    return cell;
}

//点击Cell界面跳转
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0)//测量记录
    {
        TestHistoryViewController *historyViewVc = [[TestHistoryViewController alloc] initWithNibName:@"TestHistoryViewController" bundle:nil];
        [self.navigationController pushViewController:historyViewVc animated:true];
        
    } else if (indexPath.section == 1 && indexPath.row == 0)//连接方式
    {
        VoltammterConnectionViewController * connectionViewVc =[[VoltammterConnectionViewController alloc] initWithNibName:@"VoltammterConnectionViewController" bundle:nil];
        [self.navigationController pushViewController:connectionViewVc animated:true];
    } else if(indexPath.section == 1 && indexPath.row == 1)//信息同步
    {
        VoltammeterInfoSyncViewController *infoSyncVc = [[VoltammeterInfoSyncViewController alloc] initWithNibName:@"VoltammeterInfoSyncViewController" bundle:nil];
        [self.navigationController pushViewController:infoSyncVc animated:true];
    }
}

@end
