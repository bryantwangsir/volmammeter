//
//  VoltammterConnectionViewController.m
//  voltammeter
//
//  Created by Xinyi Zhang on 2018/5/12.
//  Copyright © 2018年 汪宇豪. All rights reserved.
//

#import "VoltammterConnectionViewController.h"

extern VoltammetersGlobalConfig * appConfig;

@interface VoltammterConnectionViewController ()

@end

@implementation VoltammterConnectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpStartView];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUpStartView
{
    UIImage *wifi,*blueTooth;
    if(appConfig.connectionType == WIFI_CONECTION)
    {
        wifi=[UIImage imageNamed:@"voltammeter_selectHigh"];
        blueTooth=[UIImage imageNamed:@"voltammeter_selectNormal"];
        [_wifiTypeBtn setImage:wifi forState:UIControlStateNormal];
        [_blueTypeBtn setImage:blueTooth forState:UIControlStateNormal];
        //self.wifiTypeBtn.selected = YES;
        self.deviceTableView.hidden = YES;
        self.scanButton.hidden = YES;
    } else
    {
        blueTooth=[UIImage imageNamed:@"voltammeter_selectHigh"];
        wifi=[UIImage imageNamed:@"voltammeter_selectNormal"];
        [_wifiTypeBtn setImage:wifi forState:UIControlStateNormal];
        [_blueTypeBtn setImage:blueTooth forState:UIControlStateNormal];
        [self babyDelegate];
       // self.blueTypeBtn.selected = YES;
        //self.wifiTypeBtn.selected = NO;
        self.deviceTableView.hidden = NO;
        self.scanButton.hidden = NO;
    }
    /*
    self.navigationController.navigationBar.translucent = NO;
    self.tabBarController.tabBar.translucent = NO;
    self.scanIndicator.hidden = YES;
     [self.scanButton addTarget:self action:@selector(startScanAction) forControlEvents:UIControlEventTouchUpInside];
    */
}
- (BabyBluetooth *)baby{
    if (!_baby)
    {
        _baby = [BabyBluetooth shareBabyBluetooth];
    }
    return _baby;
}

#pragma mark -蓝牙相关的delegate
//蓝牙网关初始化和委托方法设置
-(void)babyDelegate{
    /*__weak typeof(self) weakSelf = self;
    BabyRhythm *rhythm = [[BabyRhythm alloc]init];
    
    //设置扫描到设备的委托
    [self.baby setBlockOnDiscoverToPeripherals:^(CBCentralManager *central, CBPeripheral *peripheral, NSDictionary *advertisementData, NSNumber *RSSI) {
        
        if (![weakSelf.currPeripheralArr containsObject:peripheral]) {
            [weakSelf.currPeripheralArr addObject:peripheral];
            [weakSelf.statusArray addObject:@""];
        }
        NSLog(@"%@",peripheral.name);
        if (weakSelf.startScan) {
            [weakSelf.deviceTableView reloadData];
        }
    }];
    
    [self.baby setBlockOnCentralManagerDidUpdateStateAtChannel:channelOnPeropheralView block:^(CBCentralManager *central) {
        if (!(appConfig.connectionType==WIFI_CONECTION))
        {
            if (central.state == CBCentralManagerStatePoweredOn) {
                [SVProgressHUD showInfoWithStatus:@"蓝牙已打开"];
            }
            else
            {
                [SVProgressHUD showInfoWithStatus:@"蓝牙已关闭，请打开蓝牙"];
                [weakSelf.baby cancelScan];
                [weakSelf.scanIndicator stopAnimating];
                [[ArresterGlobalManager sharedArresterGlobalManager] setBLEConnection:NO];
                
                [weakSelf deleteConnectPeri];
            }
        }
        
    }];
    //设置设备断开连接的委托
    [self.baby setBlockOnDisconnectAtChannel:channelOnPeropheralView block:^(CBCentralManager *central, CBPeripheral *peripheral, NSError *error) {
        NSLog(@"设备：%@--断开连接",peripheral.name);
        [SVProgressHUD showInfoWithStatus:[NSString stringWithFormat:@"设备：%@--断开连接",peripheral.name]];
        weakSelf.currPeripheral = nil;
        weakSelf.testController.currPeripheral = nil;
        [weakSelf.testController.coreBlueServicesArr removeAllObjects];
        [weakSelf deleteConnectPeri];
        [[ArresterGlobalManager sharedArresterGlobalManager] setBLEConnection:NO];
        
    }];
    //设置设备连接成功的委托,同一个baby对象，使用不同的channel切换委托回调
    [self.baby setBlockOnConnectedAtChannel:channelOnPeropheralView block:^(CBCentralManager *central, CBPeripheral *peripheral) {
        [weakSelf.scanIndicator stopAnimating];
        [[ArresterGlobalManager sharedArresterGlobalManager] setBlueSSid:peripheral.name];
        // [[ArresterGlobalManager sharedArresterGlobalManager] setBlueMac:peripheral.identifier.UUIDString];
        [[ArresterGlobalManager sharedArresterGlobalManager] setEquipmentSSid:nil];
        [[ArresterGlobalManager sharedArresterGlobalManager] setEquipmentMac:nil];
        [[ArresterGlobalManager sharedArresterGlobalManager] setBLEConnection:YES];
        weakSelf.statusArray[weakSelf.connectIndex] = @"已连接";
        [weakSelf.deviceTableview reloadData];
        [SVProgressHUD dismiss];
    }];
    
    //设置查找设备的过滤器
    [self.baby setFilterOnDiscoverPeripherals:^BOOL(NSString *peripheralName, NSDictionary *advertisementData, NSNumber *RSSI) {
        
        if ([peripheralName hasPrefix:@"QYBT-"] ) {
            return YES;
        }
        return NO;
        
    }];
    
    
    //设置设备连接失败的委托
    [self.baby setBlockOnFailToConnectAtChannel:channelOnPeropheralView block:^(CBCentralManager *central, CBPeripheral *peripheral, NSError *error) {
        NSLog(@"设备：%@--连接失败",peripheral.name);
        [weakSelf.scanIndicator stopAnimating];
        weakSelf.statusArray[weakSelf.connectIndex] = @"";
        [weakSelf.deviceTableview reloadData];
        weakSelf.connectIndex = -1;
        [SVProgressHUD showInfoWithStatus:[NSString stringWithFormat:@"设备：%@--断开连接",peripheral.name]];
        [[ArresterGlobalManager sharedArresterGlobalManager] setBLEConnection:NO];
        
    }];
    
    //设置发现设备的Services的委托
    [self.baby setBlockOnDiscoverServicesAtChannel:channelOnPeropheralView block:^(CBPeripheral *peripheral, NSError *error) {
        if (weakSelf.testController.coreBlueServicesArr.count > 0) {
            [weakSelf.testController.coreBlueServicesArr removeAllObjects];
        }
        [weakSelf.testController.coreBlueServicesArr addObjectsFromArray:peripheral.services];
        [rhythm beats];
    }];
    
    //扫描选项->CBCentralManagerScanOptionAllowDuplicatesKey:忽略同一个Peripheral端的多个发现事件被聚合成一个发现事件
    NSDictionary *scanForPeripheralsWithOptions = @{CBCentralManagerScanOptionAllowDuplicatesKey:@YES};
    //连接设备->
    [self.baby setBabyOptionsWithScanForPeripheralsWithOptions:scanForPeripheralsWithOptions connectPeripheralWithOptions:nil scanForPeripheralsWithServices:nil discoverWithServices:nil discoverWithCharacteristics:nil];*/
}

- (void)startScanAction
{
    if (!self.startScan) {
        //        [self.baby cancelAllPeripheralsConnection];
        //设置委托后直接可以使用，无需等待CBCentralManagerStatePoweredOn状态。
        self.baby.scanForPeripherals().begin().stop(10);
        [self.scanIndicator startAnimating];
        self.startScan = YES;
        self.scanIndicator.hidden = NO;
        self.mytimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(stopScan) userInfo:nil repeats:NO];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)wifiBtnClick:(id)sender {
    UIImage *wifi,*blueTooth;
    appConfig.connectionType=WIFI_CONECTION;
    wifi=[UIImage imageNamed:@"voltammeter_selectHigh"];
    blueTooth=[UIImage imageNamed:@"voltammeter_selectNormal"];
    [_wifiTypeBtn setImage:wifi forState:UIControlStateNormal];
    [_blueTypeBtn setImage:blueTooth forState:UIControlStateNormal];
   // self.wifiTypeBtn.selected = YES;
    self.deviceTableView.hidden = YES;
    self.scanButton.hidden = YES;
}

- (IBAction)bluetoothBtnClick:(id)sender {
    UIImage *wifi,*blueTooth;
    appConfig.connectionType=BLUTOOTH_CONECTION;
    blueTooth=[UIImage imageNamed:@"voltammeter_selectHigh"];
    wifi=[UIImage imageNamed:@"voltammeter_selectNormal"];
    [_wifiTypeBtn setImage:wifi forState:UIControlStateNormal];
    [_blueTypeBtn setImage:blueTooth forState:UIControlStateNormal];
    [self babyDelegate];
    //self.blueTypeBtn.selected = YES;
    //self.wifiTypeBtn.selected = NO;
    self.deviceTableView.hidden = NO;
    self.scanButton.hidden=NO;
}
@end
