//
//  VoltammterConnectionViewController.h
//  voltammeter
//
//  Created by Xinyi Zhang on 2018/5/12.
//  Copyright © 2018年 汪宇豪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VoltammetersGlobalConfig.h"
#import "Common.h"
#import "BabyBluetooth.h"
#import <CoreBluetooth/CoreBluetooth.h>
#define channelOnPeropheralView @"peripheralView"

@interface VoltammterConnectionViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *blueTypeBtn;
@property (weak, nonatomic) IBOutlet UIButton *wifiTypeBtn;
@property (weak, nonatomic) IBOutlet UILabel *wifiLabel;
@property (weak, nonatomic) IBOutlet UILabel *blueLabel;
@property (weak, nonatomic) IBOutlet UITableView *deviceTableView;
@property (weak, nonatomic) IBOutlet UIButton *scanButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *scanIndicator;
@property (nonatomic,assign) NSInteger connectIndex;
@property (nonatomic, assign) BOOL startScan ;
@property (nonatomic,strong) NSTimer* mytimer;
//收集到的蓝牙的数据集合
@property(nonatomic,strong) NSMutableArray<CBPeripheral*> *currPeripheralArr;

//当前外设状态的集合
@property(nonatomic,strong) NSMutableArray<NSString*> *statusArray;

//蓝牙相关
@property(nonatomic,strong) BabyBluetooth *baby;
@property (nonatomic,strong) CBPeripheral* currPeripheral;
- (IBAction)wifiBtnClick:(id)sender;
- (IBAction)bluetoothBtnClick:(id)sender;

@end

