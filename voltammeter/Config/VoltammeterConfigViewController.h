//
//  VoltammeterConfigViewController.h
//  voltammeter
//
//  Created by 汪宇豪 on 25/04/2018.
//  Copyright © 2018 汪宇豪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VoltammetersGlobalConfig.h"
#import "ImportConfigViewController.h"

@interface VoltammeterConfigViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *deviceName;
@property (weak, nonatomic) IBOutlet UITextField *plugType;
@property (weak, nonatomic) IBOutlet UITextField *PT;
@property (weak, nonatomic) IBOutlet UITextField *CT;
@property (weak, nonatomic) IBOutlet UITextField *safeThreshold;

- (IBAction)confirmBtn:(id)sender;
- (IBAction)importBtn:(id)sender;



@end
