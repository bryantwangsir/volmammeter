//
//  VoltammeterConfigViewController.m
//  voltammeter
//
//  Created by 汪宇豪 on 25/04/2018.
//  Copyright © 2018 汪宇豪. All rights reserved.
//

#import "VoltammeterConfigViewController.h"

extern VoltammetersGlobalConfig *appConfig;

@interface VoltammeterConfigViewController ()

@end

@implementation VoltammeterConfigViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    // Do any additional setup after loading the view from its nib.
}

-(void) setUpView//转入界面的初始化
{
    self.navigationController.navigationBar.translucent = NO;
    self.tabBarController.tabBar.translucent = NO;
    self.title = @"配置";
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    _CT.text = [NSString stringWithFormat:@"%lf",appConfig.CTTransformationRatio];
    _PT.text=[NSString stringWithFormat:@"%lf",appConfig.PTTransformationRatio];
    _deviceName.text=appConfig.deviceName;
    _safeThreshold.text=[NSString stringWithFormat:@"%lf",appConfig.safeThreshold];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//弹出AlertView
-(UIAlertController *) creatAlertView:(bool)isCT:(bool)isPT:(bool)isSafe
{
    NSString *title,*messeage=@"";

    if(isCT&&isPT&&isPT)
    {
        title=@"成功";
        messeage = @"配置修改成功";
    } else
    {
        title=@"发生错误";
        if(isCT==false)
        {
            messeage=[messeage stringByAppendingString:@"CT变比参数\n"];
        }
        if(isPT==false)
        {
            messeage=[messeage stringByAppendingString:@"PT变比参数\n"];
        }
        if(isSafe==false)
        {
            messeage=[messeage stringByAppendingString:@"安全告警门限参数\n"];
        }
        messeage=[messeage stringByAppendingString:@"不合法\n"];
    }
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title
                                                                             message:messeage
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"OK Action");
    }];
    [alertController addAction:okAction];
    return alertController;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
//确认按钮
-(IBAction)confirmBtn:(id)sender {
    //数据合法化检查
    bool isPTLegal=[appConfig isPTTransformationRatioLegal:_PT.text],isCTLegal=[appConfig isCTTransformationRatio:_CT.text],isSafeLegal=[appConfig isSafeThresholdLegal:_safeThreshold.text];
    UIAlertController *alertController=[self creatAlertView:isCTLegal :isPTLegal :isSafeLegal];
    if (isCTLegal&&isPTLegal&&isSafeLegal)//当参数合法时，修改参数
    {
        appConfig.CTTransformationRatio = [_CT.text doubleValue];
        appConfig.PTTransformationRatio = [_PT.text doubleValue];
        appConfig.deviceName = _deviceName.text;
        appConfig.safeThreshold=[_safeThreshold.text doubleValue];
        //生成新的jSON文件
        [appConfig saveCurrentConfigToLocal:[appConfig createjSONData]];
    } else{
        //参数非法时，Label数值重置会当前参数
        _PT.text=[NSString stringWithFormat:@"%lf",appConfig.PTTransformationRatio];
        _CT.text=[NSString stringWithFormat:@"%lf",appConfig.CTTransformationRatio];
        _safeThreshold.text=[NSString stringWithFormat:@"%lf",appConfig.safeThreshold];
        _deviceName.text=appConfig.deviceName;

    }

    [self presentViewController:alertController animated:YES completion:nil];
}
//导入配置按钮
- (IBAction)importBtn:(id)sender {
    ImportConfigViewController *importConfigVc = [[ImportConfigViewController alloc] initWithNibName:@"ImportConfigViewController" bundle:nil];
    [self.navigationController pushViewController:importConfigVc animated:true];
}
@end
