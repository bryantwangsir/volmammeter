//
//  ImportConfigViewController.m
//  voltammeter
//
//  Created by Xinyi Zhang on 2018/6/1.
//  Copyright © 2018年 汪宇豪. All rights reserved.
//

#import "ImportConfigViewController.h"

@interface ImportConfigViewController ()

@end

@implementation ImportConfigViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpStartView];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUpStartView
{
    NSLog(@"%@,%@",_localBtn.titleLabel.text , _webBtn.titleLabel.text);
    self.currentFileType = kLocalFileType;
    //将_dateContainView放置在父视图前
    [self.view bringSubviewToFront:_dateContainView];
    if(_currentFileType==kLocalFileType)
    {
        _localIcon.image = [UIImage imageNamed:@"voltammeter_local_high"];
        _webIcon.image = [UIImage imageNamed:@"voltammeter_network_normal"];
        self.localBtn.selected = true;
        self.webBtn.selected = false;
    } else
    {
        _localIcon.image = [UIImage imageNamed:@"voltammeter_local_normal"];
        _webIcon.image = [UIImage imageNamed:@"voltammeter_network_high"];
        self.localBtn.selected = false;
        self.webBtn.selected = true;
    }
    //将NavigationBar设为不透明
    self.navigationController.navigationBar.translucent = false;
    self.tabBarController.tabBar.translucent = false;
    UIView *footerView = [[UIView alloc] init];
    UIView *view = [[UIView alloc] init];
    //获取当前时间作为搜索截止时间
    self.endDate = [NSDate date];
    //设置TableView样式
    self.ConfigHistoryTableView.tableFooterView = footerView;
    self.ConfigHistoryTableView.backgroundView = view;
    //设置DatePicker样式
    self.dataPicker.datePickerMode = UIDatePickerModeDate;
    
    //设置TableView Cell样式
    /*
     [self.ConfigHistoryTableView registerNib:[UINib nibWithNibName:@"" bundle:nil] forCellReuseIdentifier:@""];
     */
    
    NSString *currentDateString = @"2016-01-01 ";

    //设置NSDate格式
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:@"yyyy-MM-DD "];
    [dateFormater setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];

    //设置最后日期
    self.lastDate = [dateFormater dateFromString:currentDateString];
    NSString *startBtn = [NSString stringWithFormat:@"%@",self.lastDate];
    NSRange rg1 = [startBtn rangeOfString:@" "];
    startBtn = [startBtn substringToIndex:rg1.location];
    [self.startDateBtn setTitle:startBtn forState:UIControlStateNormal];

    //获取当前的时间
    NSString *currentDateStr = [NSString stringWithFormat:@"%@",self.endDate];
    NSRange rg = [currentDateStr rangeOfString:@" " options:NSCaseInsensitiveSearch];
    currentDateStr = [currentDateStr substringToIndex:rg.location];
    [self.endDateBtn setTitle:currentDateStr forState:UIControlStateNormal];

}
//切换本地与网络文件模式
-(void)switchFileMode:(bool)isLocalMode
{
    if(isLocalMode)
    {
        _currentFileType=kLocalFileType;
        _localIcon.image = [UIImage imageNamed:@"voltammeter_local_high"];
        _webIcon.image = [UIImage imageNamed:@"voltammeter_network_normal"];
        self.localBtn.selected = true;
        self.webBtn.selected = false;

    } else
    {
        _currentFileType = kWebFileType;
        _localIcon.image = [UIImage imageNamed:@"voltammeter_local_normal"];
        _webIcon.image = [UIImage imageNamed:@"voltammeter_network_high"];
        self.localBtn.selected = false;
        self.webBtn.selected = true;
    }
}


- (IBAction)localBtnClick:(id)sender {
    [self switchFileMode:true];
}

- (IBAction)webBtnClick:(id)sender {
    [self switchFileMode:false];
}

- (IBAction)startBtnClick:(id)sender {
    [self.view endEditing:YES];
    self.isShowDatePicker = true;
    if(self.isShowDatePicker)
    {
        self.isShowLastDate = true;
        self.isShowCurrentDate = false;
        [UIView animateWithDuration:0.5 animations:^
        {
            self.datePickerContainConstantBottom.constant = 0;
            //[self.datePicker setDate:self.lastDate];
            self.dataLabel.text=@"开始日期";
        }];
    } else
    {
        [UIView animateWithDuration:0.5 animations:^
        {
            self.datePickerContainConstantBottom.constant = 300;
        }];
        self.isShowLastDate = false;
    }
}

- (IBAction)endDateBtnClick:(id)sender
{
    [self.view endEditing:YES];
    self.isShowDatePicker = true;
    if(self.isShowDatePicker)
    {
        self.isShowCurrentDate = true;
        self.isShowLastDate = false;
        [UIView animateWithDuration:0.5 animations:^{
            self.datePickerContainConstantBottom.constant = 0;
            //[self.datePicker setDate:self.lastDate];
            self.dataLabel.text=@"结束日期";
        }];
    } else
    {
        [UIView animateWithDuration:0.5 animations:^{
            self.datePickerContainConstantBottom.constant = 300;
        }];
        self.isShowCurrentDate = false;
    }
}
- (IBAction)datePickerCancelBtnClick:(id)sender {
    _isShowDatePicker = false;
    _datePickerContainConstantBottom.constant = 300;
    _isShowCurrentDate=false;
    _isShowLastDate = false;
}
- (IBAction)datePickerComfirmBtnClick:(id)sender {
    if(_isShowCurrentDate)//结束日期
    {
        self.endDate = self.dataPicker.date;
        NSString *startBtn = [NSString stringWithFormat:@"%@",self.endDate];
        NSRange rg = [startBtn rangeOfString:@" "];
        startBtn = [startBtn substringToIndex:rg.location];
        [self.endDateBtn setTitle:startBtn forState:UIControlStateNormal];
        [UIView animateWithDuration:0.5 animations:^{
            self.datePickerContainConstantBottom.constant = 300;
            self.isShowCurrentDate = NO;
        }];

    }else if (_isShowLastDate)//开始日期
        {
            self.lastDate = self.dataPicker.date;
            NSString *startBtn = [NSString stringWithFormat:@"%@",self.lastDate];
            NSRange rg = [startBtn rangeOfString:@" "];
            startBtn = [startBtn substringToIndex:rg.location];
            [self.startDateBtn setTitle:startBtn forState:UIControlStateNormal];
            [UIView animateWithDuration:0.5 animations:^{
                self.datePickerContainConstantBottom.constant = 300;
                self.isShowLastDate = NO;
            }];
        }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
