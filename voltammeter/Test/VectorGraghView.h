//
//  VectorGraghView.h
//  voltammeter
//
//  Created by 汪宇豪 on 2018/5/14.
//  Copyright © 2018 汪宇豪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VectorInfoModel.h"
#define VECTOR_VIEW_HEIGHT 450
@interface VectorGraghView : UIScrollView
@property (nonatomic,strong) VectorInfoModel* model;
@end
