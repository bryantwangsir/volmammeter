//
//  VoltammeterTestViewController.m
//  voltammeter
//
//  Created by 汪宇豪 on 25/04/2018.
//  Copyright © 2018 汪宇豪. All rights reserved.
//

#import "VoltammeterTestViewController.h"
#import "VoltammetersGlobalConfig.h"
#import "HMSegmentedControl.h"
#import "Common.h"
#import "IntimeTestView.h"
#import "VectorGraghView.h"
#import "HarmonicView.h"
#import "ElecInspectionView.h"
#import "SheetViewCell.h"
#import "HarModel.h"
#import "ZFChart.h"
#import "ElcModel.h"
#import "AppDelegate.h"
#import "Masonry.h"
#import "TXScrollLabelView.h"
#import "IntimeModel.h"
#import "UIButton+Corner.h"
#import "VectorInfoModel.h"

#define HEAD_VIEW_HEIGHT 59
#define SEG_HEIGHT 40
#define BOTTOM_VIEW_HEIGHT 60
#define GAP 2
#define HAR_FIRST_ROW 120
#define HAR_SEC_ROW (kScreenWidth - HAR_FIRST_ROW)
#define HAR_THIRD_ROW HAR_SEC_ROW/3
#define HAR_HEIGHT 25
#define HAR_CHART_HEIGHT 400

#define ELC_CELL_HEIGHT 35
#define ELC_CELL_FIRST_ROW_WIDTH 150
#define ELC_CELL_ROW_WIDTH (kScreenWidth - ELC_CELL_FIRST_ROW_WIDTH)/3

#define ELC_COLLECTVIEW_TAG 1001
#define HAR_COLLECVIEW_TAG 1000
#define CENTER_VIEW_RECT CGRectMake(0, 0, self.centerView.width, self.centerView.height)

extern VoltammetersGlobalConfig * appConfig;

@interface VoltammeterTestViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,ZFGenericChartDataSource,ZFBarChartDelegate,NetWorkChange>
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UIView *headView;
@property (weak, nonatomic) IBOutlet UIView *centerView;
@property (weak, nonatomic) IBOutlet UIView *segView;
@property(nonatomic,strong) IntimeTestView* intimeTestView;
@property(nonatomic,strong) VectorGraghView* vectorGraghView;
@property(nonatomic,strong) ElecInspectionView* elecInspectionView;
@property(nonatomic,strong) HarmonicView* harmonicView;
@property(nonatomic,assign) NSInteger selectViewNum;
@property(nonatomic,strong) UICollectionView* harCollectionView;
@property(nonatomic,strong) HarModel * harModel;
@property(nonatomic,strong) ElcModel * elcModel;
@property(nonatomic,strong) IntimeModel* intimeModel;
@property(nonatomic,strong) VectorInfoModel* vectorInfoModel;
@property(nonatomic,strong) ZFBarChart* barChart;
@property(nonatomic,strong) TXScrollLabelView* nameLabel;
@property(nonatomic,strong) UIImageView* selectView;

//判断谐波是用表格显示还是柱状图显示
@property(nonatomic,assign )BOOL isBar;
@property (weak, nonatomic) IBOutlet UILabel *wifiSSID;
@property(nonatomic,strong) UICollectionView* elcCollectionView;
@end

@implementation VoltammeterTestViewController

//NetWorkChange中方法实现
- (void)sendSSID:(NSString *)ssid {
    appConfig.wifiID = ssid;
    _wifiSSID.text=ssid;
}
#pragma mark - ViewCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupModel];
    [self setUpView];

    // Do any additional setup after loading the view from its nib.
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupModel
{
    self.harModel = [[HarModel alloc] init];
    self.elcModel = [[ElcModel alloc] init];
    self.intimeModel = [[IntimeModel alloc] init];
    self.vectorInfoModel = [[VectorInfoModel alloc] init];
}
- (void)setUpView
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.delegate=self;
    
    self.title = @"测量";
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    self.tabBarController.tabBar.translucent = NO;
    
    [self.startButton setCorner:5 color:[UIColor colorWithHexString:BACKGROUD_COLOR] width:1];
    [self.saveButton setCorner:5 color:[UIColor colorWithHexString:BACKGROUD_COLOR] width:1];
    
    [self.headView addSubview:self.nameLabel];
    [self.nameLabel beginScrolling];
    
    HMSegmentedControl *segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"实时数据", @"向量图", @"谐波分析",@"用电检查"]];

    segmentedControl.frame = CGRectMake(0, 0, kScreenWidth, self.segView.height);
    segmentedControl.segmentEdgeInset = UIEdgeInsetsMake(0, 10, 0, 10);
    segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.verticalDividerEnabled = YES;
    segmentedControl.verticalDividerColor = [UIColor blackColor];
    segmentedControl.verticalDividerWidth = 1.0f;
    [segmentedControl setTitleFormatter:^NSAttributedString *(HMSegmentedControl *segmentedControl, NSString *title, NSUInteger index, BOOL selected) {
        NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName : [UIColor blackColor]}];
        return attString;
    }];
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    [self.segView addSubview:segmentedControl];
   
    self.selectViewNum = 0;
    [self.centerView addSubview:self.intimeTestView];
    
    self.isBar = NO;
    [self.harmonicView addSubview:self.harCollectionView];
    
}

#pragma mark - CollectionView

//设置每个item的大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    switch (collectionView.tag) {
        case HAR_COLLECVIEW_TAG:
            if (indexPath.section == 0) {
                if (indexPath.row == 1 || indexPath.row == 3) {
                    return CGSizeMake(HAR_SEC_ROW, HAR_HEIGHT);
                }
                else
                {
                    return CGSizeMake(HAR_FIRST_ROW, HAR_HEIGHT);
                }
            }else{
                if (indexPath.row % 4 == 0) {
                    return CGSizeMake(HAR_FIRST_ROW, HAR_HEIGHT);
                }
                else
                {
                    return CGSizeMake(HAR_THIRD_ROW, HAR_HEIGHT);
                }
            }
            break;
        case ELC_COLLECTVIEW_TAG:
            if (indexPath.row % 4 == 0) {
                return CGSizeMake(ELC_CELL_FIRST_ROW_WIDTH, ELC_CELL_HEIGHT);
            }
            else{
                return CGSizeMake(ELC_CELL_ROW_WIDTH, ELC_CELL_HEIGHT);
            }
            
            break;
        default:
            return CGSizeMake(0, 0);
            break;
    }
    
}

//代理相应方法
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    switch (collectionView.tag) {
        case HAR_COLLECVIEW_TAG:
            return 2;
            break;
        case ELC_COLLECTVIEW_TAG:
            return 1;
            break;
        default:
            return 0;
            break;
    }

}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    switch (collectionView.tag) {
        case HAR_COLLECVIEW_TAG:
            switch (section) {
                case 0:
                    return 4;
                    break;
                case 1:
                    return 212;
                    break;
                default:
                    return 1;
                    break;
            }
            break;
        case ELC_COLLECTVIEW_TAG:
            return self.elcModel.tableValue.count;
            break;
        default:
            return 0;
            break;
    }
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    SheetViewCell * cell  = [collectionView dequeueReusableCellWithReuseIdentifier:@"sheetcell" forIndexPath:indexPath];
    switch (collectionView.tag) {
        case HAR_COLLECVIEW_TAG:
            if (indexPath.section == 0) {
                cell.name.text = self.harModel.fisrtSectionArray[indexPath.row];
            }
            else
            {
                cell.name.text = self.harModel.secSectionArray[indexPath.row];
            }
            break;
        case ELC_COLLECTVIEW_TAG:
            cell.name.text = self.elcModel.tableValue[indexPath.row];
            break;
        default:
            return 0;
            break;
    }
    
    return cell;
}

#pragma mark - ButtonAction
- (void)BeginTest
{
    NSLog(@"开始测量");
}

- (void)SaveResult
{
    NSLog(@"保存数据");
}

#pragma mark - barselTab

- (void)tapBarSel
{
    if (self.isBar) {
        self.isBar = NO;
        [self.barChart removeFromSuperview];
        [self.harmonicView addSubview:self.harCollectionView];
        [self.selectView setImage:[UIImage imageNamed:@"bar-chart.png"]];
    }
    else
    {
        self.isBar = YES;
        [self.harCollectionView removeFromSuperview];
        [self.harmonicView addSubview:self.barChart];
        [self.selectView setImage:[UIImage imageNamed:@"Category.png"]];
        [self.barChart strokePath];
    }
}

#pragma mark - segmentedControl
- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    NSLog(@"Selected index %ld (via UIControlEventValueChanged)", (long)segmentedControl.selectedSegmentIndex);
    
    switch (self.selectViewNum) {
        case 0:
            [self.intimeTestView removeFromSuperview];
            break;
        case 1:
            [self.vectorGraghView removeFromSuperview];
            break;
        case 2:
            [self.harmonicView removeFromSuperview];
            break;
        case 3:
            [self.elecInspectionView removeFromSuperview];
            
            break;
        default:
            break;
    }
    
    switch (segmentedControl.selectedSegmentIndex) {
        case 0:
            [self.centerView addSubview:self.intimeTestView];
            self.selectViewNum = 0;
            break;
        case 1:
            [self.centerView addSubview:self.vectorGraghView];
            self.selectViewNum = 1;
            self.vectorGraghView.model = self.vectorInfoModel;
            break;
        case 2:
            [self.centerView addSubview:self.harmonicView];
            self.selectViewNum = 2;
            break;
        case 3:
            [self.centerView addSubview:self.elecInspectionView];
            [self.elcCollectionView reloadData];
            self.selectViewNum = 3;
            break;
        default:
            break;
    }
}
#pragma mark - ZFGenericChartDataSource
- (NSArray *)valueArrayInGenericChart:(ZFGenericChart *)chart
{
    return self.harModel.chartValueArray;
}

- (NSArray *)nameArrayInGenericChart:(ZFGenericChart *)chart{
    return self.harModel.xLabelArray;
}
- (CGFloat)axisLineMaxValueInGenericChart:(ZFGenericChart *)chart
{
    return 50;
}
- (CGFloat)axisLineMinValueInGenericChart:(ZFGenericChart *)chart
{
    return 0;
}
#pragma mark - getter
- (ZFBarChart *)barChart{
    if(!_barChart)
    {
        _barChart = [[ZFBarChart alloc] initWithFrame:CGRectMake(0, BAR_VIEW_HEIGHT, kScreenWidth,kScreenHeight - (HEAD_VIEW_HEIGHT + NAV_HEIGHT + SEG_HEIGHT + 2*GAP) - TAB_HEIGHT - BOTTOM_VIEW_HEIGHT - GAP - BAR_VIEW_HEIGHT)];
        _barChart.dataSource = self;
        _barChart.delegate = self;
        _barChart.topicLabel.text = @"谐波分析";
        _barChart.unit = @"%";
    }
    return _barChart;
}
- (UICollectionView *)harCollectionView
{
    if (!_harCollectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _harCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, BAR_VIEW_HEIGHT, kScreenWidth, kScreenHeight - (HEAD_VIEW_HEIGHT + NAV_HEIGHT + SEG_HEIGHT + 2*GAP) - TAB_HEIGHT - BOTTOM_VIEW_HEIGHT - GAP - BAR_VIEW_HEIGHT) collectionViewLayout:layout];
        _harCollectionView.dataSource = self;
        _harCollectionView.delegate = self;
        _harCollectionView.backgroundColor = [UIColor whiteColor];
        [_harCollectionView registerNib:[UINib nibWithNibName:@"SheetViewCell" bundle:nil] forCellWithReuseIdentifier:@"sheetcell"];
        _harCollectionView.tag = HAR_COLLECVIEW_TAG;
        
    }
    return _harCollectionView;
}
- (HarmonicView *)harmonicView
{
    if (!_harmonicView) {
        NSArray *nibView =  [[NSBundle mainBundle] loadNibNamed:@"HarmonicView"owner:self options:nil];
        _harmonicView = [nibView objectAtIndex:0];
        _harmonicView.frame = CENTER_VIEW_RECT;
        _harmonicView.backgroundColor = [UIColor colorWithHexString:BACKGROUD_COLOR];
        UIImage *pic = [ UIImage imageNamed:@"bar-chart.png"];
        self.selectView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, BAR_VIEW_WIDTH, BAR_VIEW_HEIGHT)];
        [self.selectView setImage:pic];
        self.selectView.clipsToBounds  = YES;
        self.selectView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesturRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapBarSel)];
        [self.selectView addGestureRecognizer:tapGesturRecognizer];
        [_harmonicView addSubview:self.selectView];
        
    }
    return _harmonicView;
}
- (UICollectionView *)elcCollectionView
{
    if (!_elcCollectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _elcCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight - (HEAD_VIEW_HEIGHT + NAV_HEIGHT + SEG_HEIGHT + 2*GAP) - TAB_HEIGHT - BOTTOM_VIEW_HEIGHT - GAP) collectionViewLayout:layout];
        _elcCollectionView.dataSource = self;
        _elcCollectionView.delegate = self;
        _elcCollectionView.backgroundColor = [UIColor whiteColor];
        [_elcCollectionView registerNib:[UINib nibWithNibName:@"SheetViewCell" bundle:nil] forCellWithReuseIdentifier:@"sheetcell"];
        _elcCollectionView.tag = ELC_COLLECTVIEW_TAG;
    }
    return _elcCollectionView;
}

- (ElecInspectionView *)elecInspectionView
{
    if (!_elecInspectionView) {
        NSArray *nibView =  [[NSBundle mainBundle] loadNibNamed:@"ElecInspectionView"owner:self options:nil];
        _elecInspectionView = [nibView objectAtIndex:0];
        _elecInspectionView.frame = CENTER_VIEW_RECT;
        _elecInspectionView.backgroundColor = [UIColor colorWithHexString:BACKGROUD_COLOR];
        [_elecInspectionView addSubview:self.elcCollectionView];
    }
    return _elecInspectionView;
}
- (IntimeTestView*) intimeTestView
{
    if (!_intimeTestView) {
        NSArray *nibView =  [[NSBundle mainBundle] loadNibNamed:@"IntimeTestView"owner:self options:nil];
        _intimeTestView = [nibView objectAtIndex:0];
        _intimeTestView.frame = CENTER_VIEW_RECT;
        _intimeTestView.contentSize = CGSizeMake(kScreenWidth, 1000);
        _intimeTestView.backgroundColor = [UIColor colorWithHexString:BACKGROUD_COLOR];
    }
    return _intimeTestView;
}
- (VectorGraghView *)vectorGraghView{
    if (!_vectorGraghView) {
        NSArray *nibView =  [[NSBundle mainBundle] loadNibNamed:@"VectorGraghView"owner:self options:nil];
        _vectorGraghView = [nibView objectAtIndex:0];
        _vectorGraghView.frame = CENTER_VIEW_RECT;
        _vectorGraghView.backgroundColor = [UIColor colorWithHexString:BACKGROUD_COLOR];
        _vectorGraghView.contentSize = CGSizeMake(kScreenWidth, VECTOR_VIEW_HEIGHT);
    }
    return _vectorGraghView;
}
- (TXScrollLabelView *)nameLabel
{
    if (!_nameLabel) {
       _nameLabel =  [TXScrollLabelView scrollWithTitle:self.intimeModel.name type:TXScrollLabelViewTypeLeftRight velocity:1 options:UIViewAnimationOptionCurveEaseInOut];
        _nameLabel.frame = CGRectMake(kScreenWidth - NAME_LABEL_WIDTH, 0, NAME_LABEL_WIDTH, HEAD_VIEW_HEIGHT);
        _nameLabel.backgroundColor = [UIColor whiteColor];
        _nameLabel.scrollTitleColor = [UIColor blackColor];
    }
    return _nameLabel;
}
@end
