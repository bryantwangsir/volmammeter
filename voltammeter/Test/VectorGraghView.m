//
//  VectorGraghView.m
//  voltammeter
//
//  Created by 汪宇豪 on 2018/5/14.
//  Copyright © 2018 汪宇豪. All rights reserved.
//

#import "VectorGraghView.h"
#import "VectorView.h"
@interface VectorGraghView()
@property (weak, nonatomic) IBOutlet UILabel *freLabel;
@property (weak, nonatomic) IBOutlet UILabel *modeLabel;
@property (weak, nonatomic) IBOutlet UILabel *u1Label;
@property (weak, nonatomic) IBOutlet UILabel *u2Label;
@property (weak, nonatomic) IBOutlet UILabel *u3Label;
@property (weak, nonatomic) IBOutlet UILabel *i3Label;
@property (weak, nonatomic) IBOutlet UILabel *i2Label;
@property (weak, nonatomic) IBOutlet UILabel *i1Label;
@end
@implementation VectorGraghView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setModel:(VectorInfoModel *)model
{
    _model = model;
    self.i1Label.text = [NSString stringWithFormat:@"I1=%@A",model.i1];
    self.i2Label.text = [NSString stringWithFormat:@"I2=%@A",model.i2];
    self.i3Label.text = [NSString stringWithFormat:@"I3=%@A",model.i3];
    self.u1Label.text = [NSString stringWithFormat:@"U1=%@V",model.u1];
    self.u2Label.text = [NSString stringWithFormat:@"U2=%@V",model.u2];
    self.u3Label.text = [NSString stringWithFormat:@"U3=%@V",model.u3];
    
    self.modeLabel.text = [NSString stringWithFormat:@"%@",model.mode];
    self.freLabel.text = [NSString stringWithFormat:@"频率:%@HZ",model.fre];
    
}
@end
