//
//  SheetViewCell.h
//  voltammeter
//
//  Created by 汪宇豪 on 2018/5/16.
//  Copyright © 2018 汪宇豪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SheetViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *name;

@end
