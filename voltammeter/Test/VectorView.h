//
//  VectorView.h
//  voltammeter
//
//  Created by 汪宇豪 on 2018/5/28.
//  Copyright © 2018 汪宇豪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VectorInfoModel.h"
@interface VectorView : UIView
@property (nonatomic,strong) VectorInfoModel* model;
@end
