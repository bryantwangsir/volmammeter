//
//  HarModel.m
//  voltammeter
//
//  Created by 汪宇豪 on 2018/5/16.
//  Copyright © 2018 汪宇豪. All rights reserved.
//

#import "HarModel.h"

@implementation HarModel
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.fisrtSectionArray = @[@"信号来源",@"电压/进线电流/出线电流",@"显示方式",@"幅值/百分比"];
        NSMutableArray* temp = [NSMutableArray new];
        for (int i = 0; i < 54; i ++) {
            
            if (i == 0) {
                [temp addObjectsFromArray:@[@"",@"A相",@"B相",@"C相"]];
            }
            else if (i == 1)
            {
                [temp addObjectsFromArray:@[@"基波幅值",@"",@"",@""]];
            }
            else if (i == 2)
            {
                [temp addObjectsFromArray:@[@"THD",@"",@"",@""]];
            }
            else
            {
                [temp addObjectsFromArray:@[[NSString stringWithFormat:@"%d",i - 2],@"",@"",@""]];
            }
            
        }
        self.secSectionArray = [NSArray arrayWithArray:temp];
        
        [temp removeAllObjects];
        for(int i = 0; i < 50; i ++)
        {
            [temp addObject:[NSString stringWithFormat:@"%d",i + 1]];
        }
        self.xLabelArray = [NSArray arrayWithArray:temp];
        
        [self getChartValue];
        
    }
    return self;
}
- (void)getChartValue
{
    NSMutableArray* temp = [NSMutableArray new];
    for (int j = 0; j <50; j ++) {
        [temp addObject:@"10"];
    }
    NSArray* array1 = [NSArray arrayWithArray:temp];
    
    [temp removeAllObjects];
    for (int j = 0; j <50; j ++) {
        [temp addObject:@"20"];
    }
    NSArray* array2 = [NSArray arrayWithArray:temp];
    
    [temp removeAllObjects];
    for (int j = 0; j <50; j ++) {
        [temp addObject:@"30"];
    }
    NSArray* array3 = [NSArray arrayWithArray:temp];
    
    self.chartValueArray = [NSArray arrayWithObjects:array1,array2,array3, nil];
}
@end
