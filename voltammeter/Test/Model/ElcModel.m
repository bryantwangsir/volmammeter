//
//  ElcModel.m
//  voltammeter
//
//  Created by 汪宇豪 on 2018/5/17.
//  Copyright © 2018 汪宇豪. All rights reserved.
//

#import "ElcModel.h"

@implementation ElcModel
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self fetchTableValue];
    }
    return self;
}
- (void)fetchTableValue
{
    self.tableValue = @[@"",@"标准值",@"实际值",@"告警信号",@"相序",@"",@"",@"",@"断相",@"",@"",@"",@"电压极性",@"",@"",@"",@"进线电流极性",@"",@"",@"",@"出现电流极性",@"",@"",@"",@"漏电流",@"",@"",@"",@"窃电",@"",@"",@"",@"总功率因素",@"",@"",@"",@"A相功率因素",@"",@"",@"",@"B相功率因素",@"",@"",@"",@"C相功率因素",@"",@"",@"",@"电压不平衡度",@"",@"",@"",@"进线电流不平衡度",@"",@"",@"",@"出线电流不平衡度",@"",@"",@""];
}
@end
