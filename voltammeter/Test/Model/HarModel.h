//
//  HarModel.h
//  voltammeter
//
//  Created by 汪宇豪 on 2018/5/16.
//  Copyright © 2018 汪宇豪. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HarModel : NSObject
@property (nonatomic,strong) NSArray* fisrtSectionArray;
@property (nonatomic,strong) NSArray* secSectionArray;
@property (nonatomic,strong) NSArray* chartValueArray;
@property (nonatomic,strong) NSArray* xLabelArray;
@end
