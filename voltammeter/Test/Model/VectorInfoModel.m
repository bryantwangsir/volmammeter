//
//  VectorInfoModel.m
//  voltammeter
//
//  Created by 汪宇豪 on 2018/5/17.
//  Copyright © 2018 汪宇豪. All rights reserved.
//

#import "VectorInfoModel.h"

@implementation VectorInfoModel
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self fetchData];
    }
    return self;
}
- (void)fetchData
{
    self.mode = @"三相四线";
    self.fre = @"50";
    self.u1 = @"99.7";
    self.u2 = @"100";
    self.u3 = @"99.6";
    self.i1 = @"0.99";
    self.i2 = @"0.98";
    self.i3 = @"0.94";
    self.u1Angle = @"0";
    self.u2Angle = @"120";
    self.u3Angle = @"240";
    self.i1Angle = @"30";
    self.i2Angle = @"150";
    self.i3Angle = @"270";   
}
@end
