//
//  VectorInfoModel.h
//  voltammeter
//
//  Created by 汪宇豪 on 2018/5/17.
//  Copyright © 2018 汪宇豪. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VectorInfoModel : NSObject
@property(nonatomic,copy) NSString* fre;
@property(nonatomic,copy) NSString* mode;
@property(nonatomic,copy) NSString* u1;
@property(nonatomic,copy) NSString* u2;
@property(nonatomic,copy) NSString* u3;
@property(nonatomic,copy) NSString* i1;
@property(nonatomic,copy) NSString* i2;
@property(nonatomic,copy) NSString* i3;
@property(nonatomic,copy) NSString* u1Angle;
@property(nonatomic,copy) NSString* u2Angle;
@property(nonatomic,copy) NSString* u3Angle;
@property(nonatomic,copy) NSString* i1Angle;
@property(nonatomic,copy) NSString* i2Angle;
@property(nonatomic,copy) NSString* i3Angle;

@end
