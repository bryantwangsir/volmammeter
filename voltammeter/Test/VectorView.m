//
//  VectorView.m
//  voltammeter
//
//  Created by 汪宇豪 on 2018/5/28.
//  Copyright © 2018 汪宇豪. All rights reserved.
//

#import "VectorView.h"

@implementation VectorView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor lightGrayColor];
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    [[UIColor redColor]setFill];
    UIRectFill(CGRectMake(20, 20, 100, [_model.i1 floatValue]));
}

- (void)setModel:(VectorInfoModel *)model
{
    _model = model;
    [self setNeedsDisplay];
}

@end
