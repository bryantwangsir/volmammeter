//
//  VoltammeterComparisonViewController.m
//  voltammeter
//
//  Created by Xinyi Zhang on 2018/5/9.
//  Copyright © 2018年 汪宇豪. All rights reserved.
//

#import "VoltammeterComparisonViewController.h"
#import "ZFChart.h"
#import "Common.h"
#import "ChartModel.h"
#define CHART_HEIGHT 200
#define VAMCHART_TAG 1000
#define VFRECHART_TAG 1001
#define HAED_VIEW_HEIGHT 30
@interface VoltammeterComparisonViewController ()<ZFLineChartDelegate,ZFGenericChartDataSource>
@property (nonatomic,strong) UIScrollView* scrollview;
@property (nonatomic,strong) ZFLineChart * vAmChart;
@property (nonatomic,strong) ZFLineChart * vFreChart;
@property (nonatomic,strong) ChartModel* model;
@end

@implementation VoltammeterComparisonViewController

#pragma mark - ViewLifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self modelSetup];
    [self viewSetup];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - basic setting
- (void) modelSetup
{
    self.model = [[ChartModel alloc] init];
}

- (void) viewSetup
{
    self.title=@"历史";
    self.navigationController.navigationBar.barTintColor=[UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    self.tabBarController.tabBar.translucent = NO;
    
    [self.view addSubview:self.scrollview];
    [self.scrollview addSubview:self.vAmChart];
    [self.scrollview addSubview:self.vFreChart];
    [self.vAmChart strokePath];
    [self.vFreChart strokePath];
    
}
#pragma mark - Chart
- (NSArray *)valueArrayInGenericChart:(ZFGenericChart *)chart{
    switch (chart.tag) {
        case VAMCHART_TAG:
            return self.model.vAmChartValue;
            break;
        case VFRECHART_TAG:
            return self.model.vFreChartValue;
            break;
        default:
            return @[];
            break;
    }
}

- (NSArray *)colorArrayInGenericChart:(ZFGenericChart *)chart{
    switch (chart.tag) {
        case VAMCHART_TAG:
            return self.model.abcColor;
            break;
        case VFRECHART_TAG:
            return self.model.abcColor;
            break;
        default:
            return @[];
            break;
    }
}

- (NSArray *)nameArrayInGenericChart:(ZFGenericChart *)chart {
    switch (chart.tag) {
        case VAMCHART_TAG:
            return self.model.vAmChartXLabel;
            break;
        case VFRECHART_TAG:
            return self.model.vFreChartXLabel;
            break;
        default:
            return @[];
            break;
    }
}


#pragma mark - getter
- (UIScrollView *)scrollview{
    if (!_scrollview) {
        _scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, HAED_VIEW_HEIGHT + 10, kScreenWidth, kScreenHeight)];
        _scrollview.contentSize = CGSizeMake(kScreenWidth, 3900);
    }
    return _scrollview;
}
- (ZFLineChart *)vAmChart{
    if (!_vAmChart) {
        _vAmChart = [[ZFLineChart alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, CHART_HEIGHT)];
        _vAmChart.delegate = self;
        _vAmChart.dataSource = self;
        _vAmChart.topicLabel.text = @"电压幅值变化趋势";
        _vAmChart.unit = @"V";
        _vAmChart.tag = VAMCHART_TAG;
    }
    return _vAmChart;
}

- (ZFLineChart *)vFreChart
{
    if (!_vFreChart) {
        _vFreChart = [[ZFLineChart alloc] initWithFrame:CGRectMake(0, CHART_HEIGHT, kScreenWidth, CHART_HEIGHT)];
        _vFreChart.delegate = self;
        _vFreChart.dataSource = self;
        _vFreChart.topicLabel.text = @"电压频率变化趋势";
        _vFreChart.unit = @"HZ";
        _vFreChart.tag = VFRECHART_TAG;
    }
    return _vFreChart;
}

@end
