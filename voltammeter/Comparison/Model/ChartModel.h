//
//  ChartModel.h
//  voltammeter
//
//  Created by 汪宇豪 on 2018/5/18.
//  Copyright © 2018 汪宇豪. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface ChartModel : NSObject
@property (nonatomic,strong) NSArray<NSArray*>* vAmChartValue;
@property (nonatomic,strong) NSArray<NSArray*>* vFreChartValue;
@property (nonatomic,strong) NSArray* vAmChartXLabel;
@property (nonatomic,strong) NSArray* vFreChartXLabel;
@property (nonatomic,strong) NSArray* abcColor;
@end
