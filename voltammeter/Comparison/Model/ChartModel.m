//
//  ChartModel.m
//  voltammeter
//
//  Created by 汪宇豪 on 2018/5/18.
//  Copyright © 2018 汪宇豪. All rights reserved.
//

#import "ChartModel.h"

@implementation ChartModel
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self fetchData];
        
    }
    return self;
}

- (void) fetchData
{
    self.vAmChartValue = @[@[@"10",@"20",@"30"],@[@"30",@"20",@"10"],@[@"20",@"30",@"40"]];
    self.vFreChartValue = @[@[@"10",@"20",@"30"],@[@"30",@"20",@"10"],@[@"20",@"30",@"40"]];
    self.vAmChartXLabel = @[@"1",@"2",@"3"];
    self.vFreChartXLabel = @[@"1",@"2",@"3"];
    self.abcColor = @[[UIColor yellowColor],[UIColor greenColor],[UIColor redColor]];
}

@end
