//
//  main.m
//  voltammeter
//
//  Created by 汪宇豪 on 25/04/2018.
//  Copyright © 2018 汪宇豪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VoltammetersGlobalConfig.h"
#import "AppDelegate.h"

VoltammetersGlobalConfig *appConfig;

int main(int argc, char * argv[]) {
    @autoreleasepool {
        appConfig = [[VoltammetersGlobalConfig alloc] init];
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
