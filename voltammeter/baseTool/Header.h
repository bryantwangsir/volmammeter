//
//  Header.h
//  voltammeter
//
//  Created by Xinyi Zhang on 2018/5/22.
//  Copyright © 2018年 汪宇豪. All rights reserved.
//
#ifndef Header_h
#define Header_h
//默认选中的文件参考项
typedef enum {
    kWiFiSettingRefenceAStatus  = 0, //默认参考项A
    kWiFiSettingRefenceBStatus  = 1, //默认参考项B
    kWiFiSettingRefenceCStatus  = 2, //默认参考项C
} kWiFiSettingRefenceModel;


//默认选中的文件状态
typedef enum {
    kLocalFileType  = 0, //本地文件
    kWebFileType    = 1, //网络文件
} FileTypeStatus;

#define API_HOST @"https://moa.powerqiyi.com"
#define kUpDateURL @"v1/config/device-config"
#define kCheckDownLoadURL @"v1/sync/check"
#define kTestUpLoadURL @"v1/sync/upload"
#define kTestConfigURL  @"v1/test-config"//配置搜索接口
#define kTestHistoryURL @"v1/record" //测量记录
#define kCheckOnOff @"v1/config/app" //检查开关状态
#define kCheckStartMsg @"v1/config/activate"  // 首次启动检查更新信息

#define screen_W [UIScreen mainScreen].bounds.size.width
#define screen_H [UIScreen mainScreen].bounds.size.height

#endif /* Header_h */
