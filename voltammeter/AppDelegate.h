//
//  AppDelegate.h
//  voltammeter
//
//  Created by 汪宇豪 on 25/04/2018.
//  Copyright © 2018 汪宇豪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetwork.h"
#import <SystemConfiguration/CaptiveNetwork.h>

@protocol NetWorkChange<NSObject>

// 代理传值方法
- (void)sendSSID:(NSString *)ssid;

@end

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (weak, nonatomic) id<NetWorkChange> delegate;

@end



