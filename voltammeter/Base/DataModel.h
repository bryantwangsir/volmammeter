//
//  DataModel.h
//  voltammeter
//
//  Created by 汪宇豪 on 2018/5/14.
//  Copyright © 2018 汪宇豪. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VIData:NSObject
@property (nonatomic, copy) NSString* vTotalffectiveValue;
@end


@interface DataModel : NSObject
@property(nonatomic, strong )VIData* viData;
@end
