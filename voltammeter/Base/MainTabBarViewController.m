//
//  MainTabBarViewController.m
//  LightArrester
//
//  Created by 朱晓东 on 17/3/8.
//  Copyright © 2017年 朱晓东. All rights reserved.
//

#import "MainTabBarViewController.h"
#import "BaseNaviViewController.h"
#import "VoltammeterTestViewController.h"
#import "VoltammeterMeTableViewController.h"
#import "VoltammeterConfigViewController.h"
#import "VoltammeterComparisonViewController.h"

@interface MainTabBarViewController ()

@end

@implementation MainTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSArray *imageArray=@[@"voltammeter_test_normal",@"voltammeter_comparison_normal",@"voltammeter_peizhi_normal",@"voltammeter_mine_normal"];
    NSArray *bgImageArray=@[@"voltammeter_test_high",@"voltammeter_comparison_high",@"voltammeter_peizhi_high",@"voltammeter_mine_high"];

    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:68/255.0 green:181/255.0 blue:247/255.0 alpha:1.0], NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica" size:12.0f],NSFontAttributeName,nil] forState:UIControlStateSelected];
    
    VoltammeterTestViewController *testVc = [[VoltammeterTestViewController alloc] initWithNibName:@"VoltammeterTestViewController" bundle:nil];
    testVc.tabBarItem.title = @"测量";
    testVc.tabBarItem.image=[[UIImage imageNamed:imageArray[0]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    testVc.tabBarItem.selectedImage=[[UIImage imageNamed:bgImageArray[0]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    BaseNaviViewController *nav0 = [[BaseNaviViewController alloc]initWithRootViewController:testVc];

    VoltammeterComparisonViewController *ComparisonVc = [[VoltammeterComparisonViewController alloc] initWithNibName:@"VoltammeterComparisonViewController" bundle:nil];
    ComparisonVc.tabBarItem.title = @"历史";
    ComparisonVc.tabBarItem.image=[[UIImage imageNamed:imageArray[1]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    ComparisonVc.tabBarItem.selectedImage=[[UIImage imageNamed:bgImageArray[1]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    BaseNaviViewController *nav1 = [[BaseNaviViewController alloc]initWithRootViewController:ComparisonVc];

    VoltammeterConfigViewController *BaseVc =  [[VoltammeterConfigViewController alloc] initWithNibName:@"VoltammeterConfigViewController" bundle:nil];
    BaseVc.tabBarItem.title = @"配置";
    BaseVc.tabBarItem.image=[[UIImage imageNamed:imageArray[2]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    BaseVc.tabBarItem.selectedImage=[[UIImage imageNamed:bgImageArray[2]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    BaseNaviViewController *nav2 = [[BaseNaviViewController alloc]initWithRootViewController:BaseVc];


    VoltammeterMeTableViewController *MineVc = [[VoltammeterMeTableViewController alloc] initWithNibName:@"VoltammeterMeTableViewController" bundle:nil];
    MineVc.tabBarItem.title = @"我的";
    MineVc.tabBarItem.image=[[UIImage imageNamed:imageArray[3]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    MineVc.tabBarItem.selectedImage=[[UIImage imageNamed:bgImageArray[3]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    BaseNaviViewController *nav3 = [[BaseNaviViewController alloc]initWithRootViewController:MineVc];

    self.viewControllers = @[nav0,nav1,nav2,nav3];
    self.selectedIndex = 0; //通过加到标签栏视图控制上的索引来确定
    
    [[UITabBar appearance] setTintColor:[UIColor blackColor]];
}



@end
